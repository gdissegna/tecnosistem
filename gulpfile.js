var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function(mix) {
    mix.sass([
        'foundation.scss',
        'normalize.scss',
        'admin.scss'
    ],'public/css/admin.css');
});

elixir(function(mix) {
    mix.scripts([
        'vendor/jquery.js',
        'vendor/jquery.cookie.js',
        'vendor/fastclick.js',
        'vendor/placeholder.js',
        'foundation.js',
        'dropzone.js'
        ],'public/js/admin.js');

    });