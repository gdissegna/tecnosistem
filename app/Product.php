<?php

namespace App;

use App\Http\Requests\Request;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * Class Product
 * @package App
 */
class Product extends Model
{
    use Translatable;

    const PRIMARY_PATH = 'images/products/';
    const FILES_PATH = 'files/';

    protected $fillable = ['name','codice','primary_img', 'link', 'description','category_id','inHome','video','pdf'];
    public $translatedAttributes = ['name','description','slug'];



    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->primary_img != null) {
                if (\File::isFile($model->primary_img)) {
                    \File::delete($model->primary_img);
                }
            }
            if ($model->pdf != null) {
                if (\File::isFile($model->pdf)) {
                    \File::delete($model->pdf);
                }
            }
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function createProduct(Request $request)
    {
        if ($request->hasFile('image')) {
            $imagepath = $this->saveImage($request['image']);
        } else {
            $imagepath = $this->primary_img;
        }
        if ($request->has('pdf')) {
            $pdfpath = $this->savePdf($request['pdf']);
        } else {
            $pdfpath = $this->pdf;
        }

        $data = [
            'codice' => $request['codice'],
            'primary_img' => $imagepath,
            'pdf' => $pdfpath,
            'inHome' => $request->input('inhome','0'),
            'category_id' => $request['category'],
            'video' => $request['video'],
            'it' => ['name' => $request['name_it'],'description' => $request['description_it']],
            'en' => ['name' => $request['name_en'],'description' => $request['description_en']],
        ];

        $this->create($data);

        return $this;
    }


    /**
     * test
     * @param Request $request
     * @return $this
     */
    public function updateProduct(Request $request)
    {
        if ($request->hasFile('image')) {
            $this->deleteImage();
            $newimage = $this->saveImage($request['image']);
        } else {
            $newimage = $this->primary_img;
        }
        if ($request->hasFile('pdf')) {
            $this->deletePdf();
            $newpdf = $this->savePdf($request['pdf']);
        } else {
            $newpdf = $this->pdf;
        }
        if($request['video']  ) {
            $video =  $request['video'];
        } else {
            $video = $this->video;
        }

        $data = [
            'primary_img' => $newimage,
            'pdf' => $newpdf,
            //'name' => $request['name'],
            'video' => $this->video,
            'inHome' => $request->input('inhome','0'),
            'category_id' => $request['category'],
            'it' => ['name' => $request['name_it'],'description' => $request['description_it']],
            'en' => ['name' => $request['name_en'],'description' => $request['description_en']],
        ];

        $this->fill($data)->save();

        return $this;
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    protected function saveImage(UploadedFile $image)
    {

        $filename = $image->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = self::PRIMARY_PATH . $newfilename ;
        Image::make($image)->fit(960, 640)->save($filepath);
        return $filepath;
    }

    /**
     * @param UploadedFile $pdf
     * @return string
     */
    protected function savePdf(UploadedFile $pdf)
    {
        $filename = $pdf->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = public_path() ."/". self::FILES_PATH;
        $pdf->move($filepath,$newfilename);

        $filepath = $filepath . $newfilename;

        return $filepath;
    }

    /**
     * Delete the image file
     */
    public function deleteImage()
    {
        if ($this->image != null) {
            if (\File::isFile($this->image)) {
                \File::delete($this->image);
            }
        }
    }

    /**
     * Delete pdf
     */
    protected function deletePdf()
    {
        if ($this->pdf != null) {
            if (\File::isFile($this->pdf)) {
                \File::delete($this->pdf);
            }
        }
    }


}
