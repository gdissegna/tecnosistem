<?php

namespace App\Jobs;

use Mail;
use App\Jobs\Job;
use Illuminate\Http\Request;
use Illuminate\Contracts\Bus\SelfHandling;

class ContactEmailJob extends Job implements SelfHandling
{
    /**
     * Create a new job instance.
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('mail.Contact', ['request' => $this->request], function($message) {
            $message->from('info@triangolo.it', 'Tecno Machine');
            $message->to($this->request['email'], $this->request['name'] . $this->request['cognome'] );
            $message->subject('Tecno machine, ' .  trans('custom.contact_subject'));
        });

        Mail::send('mail.contattiinterna', ['request' => $this->request], function($message) {
            $message->from('info@triangolo.it', 'Tecno Machine');
            $message->to('info@triangolo.it');
            $message->subject('Tecno machine nuovo contatto');
        });
    }
}
