<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class ArticleTranslation extends Model Implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to' => 'slug',
    );

    public $timestamps = false;
    protected $fillable = ['title', 'body','slug'];

    public function article() {
        return $this->belongsTo('App\Article','article_id');
    }

    public function getBySlug($slug) {
        return $this->where('slug', $slug)->first();
    }
}
