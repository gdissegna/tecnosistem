<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\SliderCreateRequest;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Slider extends Model
{

    const SLIDER_FOLDER = 'images/slider/';

    use SluggableTrait;

    protected $fillable = ['path'];

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->path != null) {
                if (\File::isFile($model->path)) {
                    \File::delete($model->path);
                }
            }
        });
    }

    /**
     * @param SliderCreateRequest $request
     */
    public static function saveSlider(SliderCreateRequest $request)
    {

        $slider = new Slider();

        $file = $request['file'];

        $name = $file->getClientOriginalName();
        $filename = time() . $name;
        $path = self::SLIDER_FOLDER . $filename;
        \Image::make($file)->resizeCanvas(null, 600)->save($path);

        $slider->path = $path;
        $slider->save();

    }
}
