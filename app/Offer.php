<?php

namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Offer extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $fillable = ['title','description','link','pdf','primary_img'];
    public $translatedAttributes = ['title','description','slug'];
    public $timestamps = true;


    const PRIMARY_PATH = 'images/offer/';
    const FILES_PATH = 'files/';

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->primary_img != null) {
                if (\File::isFile($model->primary_img)) {
                    \File::delete($model->primary_img);
                }
            }
            if ($model->pdf != null) {
                if (\File::isFile($model->pdf)) {
                    \File::delete($model->pdf);
                }
            }
        });
    }


    public function createOffer(Request $request)
    {

        if ($request->hasFile('image')) {
            $imagepath = $this->saveImage($request['image']);
        } else {
            $imagepath = null;
        }
        if ($request->hasFile('pdf')) {
            $pdfpath = $this->savePdf($request['pdf']);
        } else {
            $pdfpath = null;
        }

        $data = [
            'primary_img' => $imagepath,
            'pdf' => $pdfpath,
            'it' => ['title' => $request['title_it'], 'description' => $request['description_it']],
            'en' => ['title' => $request['title_en'], 'description' => $request['description_en']],
        ];

        $this->create($data);

        return $this;
    }

    public function updateOffer(Request $request)
    {

        if ($request->hasFile('image')) {
            $this->deleteImage($this->image);
            $newimage = $this->saveImage($request['image']);
        } else {
            $newimage = $this->image;
        }
        if ($request->hasFile('pdf')) {
            $this->deletePdf($this->pdf);
            $newpdf = $this->savePdf($request['pdf']);
        } else {
            $newpdf = $this->pdf;
        }

        $data = [
            'primary_img' => $newimage,
            'pdf' => $newpdf,
            'it' => ['title' => $request['title_it'], 'description' => $request['description_it']],
            'en' => ['title' => $request['title_en'], 'description' => $request['description_en']],
            'de' => ['title' => $request['title_de'], 'description' => $request['description_de']],
            'ru' => ['title' => $request['title_ru'], 'description' => $request['description_ru']]
        ];

        $this->fill($data)->save();

        return $this;
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    protected function saveImage(UploadedFile $image)
    {

        $filename = $image->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = self::PRIMARY_PATH . $newfilename;
        Image::make($image)->fit(1000, 500, function ($constraint) {
            $constraint->upsize();
        })->save($filepath);

        return $filepath;
    }

    /**
     * @param UploadedFile $pdf
     * @return string
     */
    protected function savePdf(UploadedFile $pdf)
    {
        $filename = $pdf->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = public_path() ."/". self::FILES_PATH;
        $pdf->move($filepath,$newfilename);
        $filepath = $filepath . $newfilename;

        return $filepath;
    }

    /**
     * Delete the image file
     */
    public function deleteImage()
    {
        if ($this->image != null) {
            if (\File::isFile($this->image)) {
                \File::delete($this->image);
            }
        }
    }

    /**
     * Delete pdf
     */
    protected function deletePdf()
    {
        if ($this->pdf != null) {
            if (\File::isFile($this->pdf)) {
                \File::delete($this->pdf);
            }
        }
    }

}
