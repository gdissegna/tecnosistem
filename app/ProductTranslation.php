<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ProductTranslation extends model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
    );

    public $timestamps = false;
    protected $fillable = ['name','slug','description'];

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug) {

        return $this->where('slug', '=', $slug)->first();
    }

}
