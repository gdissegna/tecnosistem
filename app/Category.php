<?php

namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Category extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $data;

    const IMAGE_FOLDER = "images/category/";

    protected $fillable = ['name', 'description', 'slug', 'image'];
    public $translatedAttributes = ['name', 'description', 'slug'];


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->image != null) {
                if (\File::isFile($model->image)) {
                    \File::delete($model->image);
                }
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }


    public function parseCategoryRequest(Request $request)
    {

        $newimage = $this->saveImage($request['image']);
        $data = [
            'image' => $newimage,
            'it' => ['name' => $request['name_it'], 'description' => $request['description_it']],
            'en' => ['name' => $request['name_en'], 'description' => $request['description_en']],
        ];

        return $data;
    }

    /**
     * @param UploadedFile $image
     * @return string $newimagepath
     */
    public function saveImage(UploadedFile $image)
    {

        $imagename = $image->getClientOriginalName();
        $newimagename = time() . $imagename;
        $newimagepath = self::IMAGE_FOLDER . $newimagename  ;
        \Image::make($image)->save($newimagepath);

        return $newimagepath;
    }

    public function deleteImage()
    {
        if ($this->image != null) {
            if (\File::isFile($this->image)) {
                \File::delete($this->image);
            }
        }
    }


    public function updateCategory(Request $request)
    {

        if ($request->hasFile('image')) {
            $this->deleteImage($this->image);
            $newimage = $this->saveImage($request['image']);
        } else {
            $newimage = $this->image;
        }
        $data = [
            'image' => $newimage,
            'it' => ['name' => $request['name_it'], 'description' => $request['description_it']],
            'en' => ['name' => $request['name_en'], 'description' => $request['description_en']],
            'de' => ['name' => $request['name_de'], 'description' => $request['description_de']],
            'ru' => ['name' => $request['name_ru'], 'description' => $request['description_ru']]

        ];

        $this->fill($data)->save();

    }


}
