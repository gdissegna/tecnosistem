<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use App\Category;

class CategoryTranslation extends Model implements SluggableInterface
{
    use SluggableTrait;

    public $timestamps = false;
    protected $fillable = ['name', 'description','slug'];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to' => 'slug',
        'on_update' => true,
    );

    public function getBySlug($slug) {

        return $this->where('slug', '=', $slug)->first();
    }


    public function category() {

        return $this->belongsTo('App\Category','category_id');
    }

}
