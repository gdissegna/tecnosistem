<?php

namespace App;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Request;
use Image;


/**
 * Class ProductImage
 * @property string image
 * @package App
 */
class ProductImage extends Model
{

    const PRIMARY_PATH = 'images/product';

    protected $fillable = ['image','alttext', 'product_id'];

    public function Product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            if ($model->image != null) {
                if (\File::isFile($model->image)) {
                    \File::delete($model->image);
                }
            }
        });
    }




    public function createImage(Request $request)
    {
        $path = $this->saveImage($request['image']);
        $this->image = $path;
        $this->alttext = $request['alttext'];
        return $this;
    }

    protected function saveImage(UploadedFile $image)
    {

        $filename = $image->getClientOriginalName();
        $newfilename = time() . $filename;
        $filepath = self::PRIMARY_PATH . $newfilename;
        Image::make($image)->save($filepath);

        return $filepath;
    }

}
