<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('', ['as' => '', 'uses' => '']);

Route::group(['middleware' => 'auth'], function () {
    // User Routes
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');
    Route::get('auth/listusers', 'Auth\UserController@listUsers');
    Route::get('auth/adduser', 'Auth\UserController@getNewUser');
    Route::post('auth/adduser', 'Auth\UserController@postNewUser');
    Route::post('auth/deleteuser/{id}', 'Auth\UserController@deleteUser');
    // Slider Routes
    Route::get('admin', ['as' => 'admin', 'uses' => 'admin\SliderController@index']);
    Route::get('admin/slider/create', ['as' => 'SliderCreate', 'uses' => 'admin\SliderController@create']);
    Route::post('admin/slider/create', ['as' => 'SliderCreatePost', 'uses' => 'admin\SliderController@store']);
    Route::get('admin/slider/{id}/edit', ['as' => 'SliderEdit', 'uses' => 'admin\SliderController@edit']);
    Route::post('admin/slider/{id}/edit', ['as' => 'SliderEditPost', 'uses' => 'admin\SliderController@update']);
    Route::get('admin/slider/{id}/delete', ['as' => 'SliderDelete', 'uses' => 'admin\SliderController@destroy']);

    // Routes Articoli
    Route::get('admin/articoli', ['as' => 'articles', 'uses' => 'admin\ArticlesController@index']);
    Route::get('admin/articoli/create', ['as' => 'ArticleCreate', 'uses' => 'admin\ArticlesController@create']);
    Route::post('admin/articoli/create', ['as' => 'ArticleCreatePost', 'uses' => 'admin\ArticlesController@store']);
    Route::get('admin/articoli/{id}/edit', ['as' => 'ArticleEdit', 'uses' => 'admin\ArticlesController@edit']);
    Route::post('admin/articoli/{id}/edit', ['as' => 'ArticleEditPost', 'uses' => 'admin\ArticlesController@update']);
    Route::get('admin/articoli/{id}/delete', ['as' => 'ArticleDelete', 'uses' => 'admin\ArticlesController@destroy']);

    // Routes Articoli
    Route::get('admin/offerte', ['as' => 'Offers', 'uses' => 'admin\OffersController@index']);
    Route::get('admin/offerte/create', ['as' => 'OfferCreate', 'uses' => 'admin\OffersController@create']);
    Route::post('admin/offerte/create', ['as' => 'OfferCreatePost', 'uses' => 'admin\OffersController@store']);
    Route::get('admin/offerte/{id}/edit', ['as' => 'OfferEdit', 'uses' => 'admin\OffersController@edit']);
    Route::post('admin/offerte/{id}/edit', ['as' => 'OfferEditPost', 'uses' => 'admin\OffersController@update']);
    Route::get('admin/offerte/{id}/delete', ['as' => 'OfferDelete', 'uses' => 'admin\OffersController@destroy']);

    // Routes Categorie
    Route::get('admin/categorie', ['as' => 'categories', 'uses' => 'admin\CategoriesController@index']);
    Route::get('admin/categorie/create', ['as' => 'CategoryCreate', 'uses' => 'admin\CategoriesController@create']);
    Route::post('admin/categorie/create', ['as' => 'CategoryCreatePost', 'uses' => 'admin\CategoriesController@store']);
    Route::get('admin/categorie/{id}/edit', ['as' => 'CategoryEdit', 'uses' => 'admin\CategoriesController@edit']);
    Route::post('admin/categorie/{id}/edit', ['as' => 'CategoryEditPost', 'uses' => 'admin\CategoriesController@update']);
    Route::get('admin/categorie/{id}/delete', ['as' => 'CategoryDelete', 'uses' => 'admin\CategoriesController@destroy']);

    Route::get('admin/prodotti', ['as' => 'products', 'uses' => 'admin\ProductsController@index']);
    Route::get('admin/prodotti/create', ['as' => 'ProductCreate', 'uses' => 'admin\ProductsController@create']);
    Route::post('admin/prodotti/create', ['as' => 'ProductCreatePost', 'uses' => 'admin\ProductsController@store']);
    Route::get('admin/prodotti/{id}/edit', ['as' => 'ProductEdit', 'uses' => 'admin\ProductsController@edit']);
    Route::post('admin/prodotti/{id}/edit', ['as' => 'ProductEditPost', 'uses' => 'admin\ProductsController@update']);
    Route::get('admin/prodotti/{id}/delete', ['as' => 'ProductDelete', 'uses' => 'admin\ProductsController@destroy']);

    Route::get('admin/prodotti/{id}/images', ['as' => 'ProductImages', 'uses' => 'admin\ProductImagesController@index']);
    Route::get('admin/prodotti/{id}/images/create', ['as' => 'ProductImagesCreate', 'uses' => 'admin\ProductImagesController@create']);
    Route::post('admin/prodotti/{id}/images/create', ['as' => 'ProductImagesCreatePost', 'uses' => 'admin\ProductImagesController@store']);
    Route::get('admin/prodotti/{slug}/images/{id}/edit', ['as' => 'ProductImagesEdit', 'uses' => 'admin\ProductImagesController@edit']);
    Route::post('admin/prodotti/{slug}/images/{id}/edit', ['as' => 'ProducImagesEditPost', 'uses' => 'admin\ProductImagesController@update']);
    Route::get('admin/prodotti/{slug}/images/{id}/delete', ['as' => 'ProductImagesDelete', 'uses' => 'admin\ProductImagesController@destroy']);
    // PDF dovnload
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// FrontEnd Routes...
Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
    Route::get('/offerta/{slug}', ['as' => 'offerta', 'uses' => 'IndexController@offerta']);
    Route::get('/offerta/download/{id}', ['as' => 'offertadownload', 'uses' => 'IndexController@offerta_download']);
    Route::get('/azienda', ['as' => 'azienda', 'uses' => 'StaticPagesController@azienda']);
    Route::get('/news', ['as' => 'newslist', 'uses' => 'NewsController@index']);
    Route::get('/news/{slug}', ['as' => 'singlenews', 'uses' => 'NewsController@show']);
    Route::get('/prodotti/{slug}/lista', ['as' => 'categoria', 'uses' => 'ProductsController@index']);
    Route::get('/prodotti/{slug}/{name}', ['as' => 'prodotto', 'uses' => 'ProductsController@prodotto']);
    Route::get('/prodotto/{id}/download', ['as' => 'prodottoDownload', 'uses' => 'ProductsController@download']);
    Route::get('/contatti', ['as' => 'contatti', 'uses' => 'ContactController@index']);
    Route::post('/contatti', ['as' => 'contattipost', 'uses' => 'ContactController@send']);
    Route::get('/carrello', ['as' => 'carrello', 'uses' => 'CartController@showcartform']);
    Route::post('/carrello', ['as' => 'carrellopost', 'uses' => 'CartController@postcartform']);
    Route::get('/carrello/{id}/add', ['as' => 'carrelloAddItem', 'uses' => 'CartController@additem']);
    Route::get('/carrello/{id}/remove', ['as' => 'carrelloRemoveItem', 'uses' => 'CartController@removeitem']);
    Route::get('/privacy', ['as' => 'privacy', 'uses' => 'StaticPagesController@privacy']);
    Route::get('/note-legali', ['as' => 'note_legali', 'uses' => 'StaticPagesController@note_legali']);
    Route::get('/assistenza', ['as' => 'assistenza', 'uses' => 'StaticPagesController@assistenza']);
    Route::post('/newsletter', ['as' => 'newsletterpost', 'uses' => 'ContactController@subscribe']);
});


