<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaticPagesController extends Controller
{
    /**
     * Displays the Azienda static page
     *
     * @return Response
     */
    public function azienda()
    {
        return view('chisiamo');
    }

    public function privacy() {
        return view('privacy');
    }

    public function assistenza() {

        return view('assistenza');
    }

    public function note_legali(){
        return view('privacy');
    }



}
