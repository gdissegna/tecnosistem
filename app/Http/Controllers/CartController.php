<?php

namespace App\Http\Controllers;

use Session;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers;
use App\Http\Requests\CartFormRequest;
use App\Jobs\SendCartEmail;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     * @var array $cart
     * @return Response
     */
    public function showcartform()
    {
        $cart = Session::get('cart');

        $cartitems = Product::whereIn('id', $cart)->get();

        return view('cart')->with('cartitems', $cartitems);
    }

    public function postcartform(CartFormRequest $request)
    {
        $cartitems = Session::get('cart');

        $cart = Product::whereIn('id',$cartitems)->get();

        $job = (new SendCartEmail($request,$cart));

        $this->dispatch($job);

        Session::forget('cart');

        Session::flash('success', 'La operazione é stata completata con successo');

        return redirect()->route('carrello');
    }


    /**
     * Add a product to the cart
     *
     */
    public function additem($id)
    {
        Session::push('cart', $id);

        Session::flash('product_added', trans('custom.prodotto_aggiunto'));

        return redirect()->back();
    }

    /**
     * remove a product from the cart
     *
     */
    public function removeitem($id)
    {
        Session::pull('cart', $id);
        return redirect()->back()->withInput();
    }

}
