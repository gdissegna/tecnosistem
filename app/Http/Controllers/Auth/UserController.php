<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;

class UserController extends Controller
{

    Protected $data;

    public function listUsers()
    {
        $users = User::all();

        return view('auth.newuser')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return null
     */
    public function getNewUser()
    {
       return view('auth.newuser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return redirect
     */
    public function PostNewUser(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        return redirect()->route('sliders');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return view('auth.newuser');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

}
