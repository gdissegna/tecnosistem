<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       $articles = Article::orderBy('created_at','desc')->paginate(9);

       return view('articoli')->with('articles', $articles);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
        //$articles = Article::orderBy('created_at','desc')->get();
        $articles_date = Article::all()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y');
        });
        $this->translations = new ArticleTranslation;

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $article = Article::findOrFail($translation->article_id);

       return view('articolo')->with('article', $article)
                              ->with('articles_date',$articles_date);
    }


}
