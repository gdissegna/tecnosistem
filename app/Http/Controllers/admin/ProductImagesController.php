<?php

namespace App\Http\Controllers\admin;


use App\Http\Requests\ProductImageCreateRequest;
use App\Http\Requests\ProductImageEditRequest;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param $id
     * @return Response
     */
    public function index($id)
    {
        $product = Product::with('images')->where('id', $id)->first();

        return view('admin.products.images')->with('product', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        $product = Product::with('images')->where('id', $id)->first();

        return view('admin.products.imagescreate')->with('product', $product);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(ProductImageCreateRequest $request, $id)
    {
        $product = Product::with('images')->where('id', $id)->first();
        $image = new ProductImage();
        $newimage = $image->createImage($request);
        $product->images()->save($newimage);

        return redirect()->route('ProductImages',[$product]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($slug, $id)
    {
        $image = ProductImage::findOrFail($id);

        return view('admin.products.imagesedit')->with('image', $image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(ProductImageEditRequest $request, $slug, $id)
    {
        $image = ProductImage::findOrFail($id);

        $path = $image->saveImage($request['image']);
        $image->image = $path;
        $image->save();

        session('message', 'image updated');

        return view('admin.products.imagesedit')->with('image', $image);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($slug, $id)
    {
        $product = Product::findOrFail($slug);

        $image = ProductImage::findOrFail($id);
        $image->delete();

        return redirect()->route('ProductImages',$product->id);
    }
}
