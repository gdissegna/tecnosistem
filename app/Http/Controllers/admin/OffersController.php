<?php

namespace App\Http\Controllers\admin;

use App\Http\Requests\OfferCreateRequest;
use App\Http\Requests\OfferEditRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Offer;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $offers = Offer::all();

        return view('admin.offers.offers')->with('offers', $offers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(OfferCreateRequest $request)
    {
        $offer = new Offer();
        $offer->createOffer($request);

        $offers = Offer::all();

        return redirect()->route('Offers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param OfferEditRequest $request
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $offer = Offer::findOrFail($id);

        return view('admin.offers.edit')->with('offer', $offer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(OfferEditRequest $request, $id)
    {
        $offer = Offer::findOrFail($id);

        $offer->updateOffer($request);

        return redirect()->route('Offers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $offer = Offer::findOrFail($id);

        $offer->delete();

        return redirect()->route('Offers');
    }
}
