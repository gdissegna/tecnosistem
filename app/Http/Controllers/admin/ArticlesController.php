<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Requests\ArticleCreateRequest;
use App\Http\Requests\ArticleEditRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

/**
 * Class ArticlesController
 * @package App\Http\Controllers\Admin
 */
class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index()
    {
        $articles = Article::all();

        return view('admin.articles.articles')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ArticleCreateRequest $request
     * @return view()
     */
    public function store(ArticleCreateRequest $request)
    {
        $article = new Article();
        $article->saveArticle($request);

        $articles = Article::all();

        return view('admin.articles.articles')->with('articles', $articles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::findOrfail($id);

        return view('admin.articles.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArticleEditRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(ArticleEditRequest $request, $id)
    {
        $article = Article::findOrfail($id);
        $article->updateArticle($request);

        Session::flash('success', 'articolo modificato correttamente');

        return redirect()->route('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->deleteArticle();

        return redirect()->route('articles');
    }
}
