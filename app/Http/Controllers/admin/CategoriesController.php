<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryEditRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.categories')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  CategoriesController $request
     * @return Response
     */
    public function store(CategoryCreateRequest $request)
    {
        $category = new Category();
        $data = $category->parseCategoryRequest($request);
        $category->create($data);

        return redirect()->route('categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryEditRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(CategoryEditRequest $request, $id)
    {
        $category = Category::find($id);
        $test = $category->updateCategory($request);
        \Session::flash('success', 'categoria editata con successo');

        return redirect()->route('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return redirect()->route('categories');
    }
}
