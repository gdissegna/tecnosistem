<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryTranslation;
use App\Product;
use App\ProductTranslation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class ProdottiController
 * @property CategoryTranslation translations
 * @package App\Http\Controllers
 */
class ProdottiController extends Controller
{

    protected $translations;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($slug)
    {
        $this->translations = new CategoryTranslation;

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $category = Category::with('products')->first($translation->category_id));

        return view('prodotti')->with('category',$category);
    }

    /**
     * @param $slug
     * @param $name
     * @return view
     */
    public function prodotto($slug,$name)
    {

        $this->translations = new CategoryTranslation;

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $category = Category::with('products')->first($translation->category_id));



        return view('prodotto')->with('category',$category)->with('product',$product);
    }

}
