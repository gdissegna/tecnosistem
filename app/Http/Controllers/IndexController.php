<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Offer;
use App\OfferTranslation;
use App\Product;
use App\Slider;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $sliders = Slider::all();
        $categories = Category::all();
        $articles = Article::OrderBy('created_at','desc')->take(3)->get();
        $products = Product::where('inHome' , 1 )->get();
        $offers = Offer::orderBy('created_at','asc')->take(2)->get();

        return view('home')->with('sliders', $sliders)
            ->with('categories', $categories)
            ->with('articles', $articles)
            ->with('products', $products)
            ->with('offers', $offers);
    }

    public function offerta($slug) {

        $this->translations = new OfferTranslation();

        $translation = $this->translations->getBySlug($slug);

        if ( ! $translation)
        {
            return App::abort(404);
        }

        $category = Offer::where('id', $translation->offer_id)->first();

        dd($category);
    }

    public function offerta_download($id) {

        $offer = Offer::findOrFail($id);

        $download_path =  $offer->pdf;

        return response()->download($download_path);
    }


}
