<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\NewsletterRequest;
use App\Jobs\ContactEmailJob;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use NZTim\Mailchimp\Mailchimp;
use NZTim\Mailchimp\Member;
use Illuminate\Translation\Translator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('contatti');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContactFormRequest $request
     * @return Response
     */
    public function send(ContactFormRequest $request)
    {
        //dd($request);
        $this->dispatch(new ContactEmailJob($request));

        \Session::flash('success', trans('email.success'));

        return redirect()->route('contatti');
    }

    public function subscribe(NewsletterRequest $request)
    {
        // Adds/updates an existing subscriber:
        Mailchimp::subscribe($request['listID'], $request['email'], $merge = ['FNAME' => $request['name']], $confirm = true);
        // Use $confirm = false to skip double-opt-in if you already have permission.
        // This method will update an existing subscriber and will not ask an existing subscriber to re-confirm.

        return redirect()->route('index');
    }

}
