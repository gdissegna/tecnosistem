<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29/10/2015
 * Time: 08:11
 */

namespace app\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Category;

class MasterComposer
{

    public function compose(View $view) {
        $menu_categories = Category::all();

        $view->with('menu_categories', $menu_categories);
    }

}