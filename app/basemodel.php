<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * Class basemodel
 * @package App
 */
class Basemodel extends Model
{
    use SluggableTrait;
    use \Dimsav\Translatable\Translatable;
}
