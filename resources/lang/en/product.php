<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_codice'     => 'Code:',
    'titolo_descrizione'     => 'Description - Technical details',
    'titolo_img_aggiuntive' => 'Images Gallery',
    'titolo_video'     => 'Our video',
    'titolo_pdf'     => 'PDF sheet',
    'carrello_button' => 'Add product to request',
    'prodotto_aggiunto' => 'Product added',
   

];