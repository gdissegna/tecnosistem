<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'Our Company',
    'titolo'     => 'Welcome to Tecno Machine Srl',
    'sottotitolo_azienda'     => 'Sale and support of used woodworking machinery',
    'testo_azienda' => 'Tecno Machine offers its customers sales and after-sales services, technical support, supply of spare parts operating since 1979 on woodworking market.',
         
   

];