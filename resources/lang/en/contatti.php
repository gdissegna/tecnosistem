<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'contatti_title' => 'Contact us',
    'contatti_form_title' => 'Information Request',
    'contatti_form_header' => 'For further information, please fill in the form hereunder. Our staff will contact you. Fields marked with * are mandatory. ',
    'contatti_subtitle' => 'Write or phone, our Staff is at your disposal',
    'contatti_indirizzo' => 'Address',
    'contatti_contatti' => 'Contact us',
    'contatti_telefono' => 'Phone',
    'label_nome' => 'First and last Name',
    'label_societa' => 'Company',
    'label_indirizzo' => 'Address',
    'label_citta' => 'City',
    'label_paese' => 'Country',
    'label_telefono' => 'Phone',
    'label_fax' => 'Fax',
    'label_email' => 'Email',
    'label_attivita' => 'Type of activity',
    'label_rivenditore' => 'Reseller',
    'label_utilizzatore' => 'User',
    'label_costruttore' => 'Manufacturer',
    'label_messaggio' => 'Message',
    'label_privacy' => 'Privacy',
    'label_privacy_testo' => 'According to the Legislative Decree n. 196/2003 (personal data protection code), we inform you that your personal data will be processed with the purpose                               of providing the services required (website surfing, information request, sign-in for services) and of sending advertising and promotion information                                     and/or material. Tecnomecsrl S.r.l. IS RESPONSIBLE FOR THE DATA PROCESSING. You may exercise the rights provided by art.7 of Legislative Decree n.                                       196/2003 by writing to: Tecnomecsrl S.r.l. P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Phone +39.0481.676679, Fax +39.0481.676681,                               E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'I hereby grant my consent to the processing of my personal data according to the above stated purposes. *',
    'label_invia' => 'Send now',
         
   

];