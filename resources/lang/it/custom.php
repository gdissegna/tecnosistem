<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'logo'     => 'Logo',
    
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'Tecno Machine Srl - vendita e riparazione macchine utensili per la lavorazione del legno',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   
    
    
   /*
   |--------------------------------------------------------------------------
   | Header
   |--------------------------------------------------------------------------
   */

    'titolo_telefono' => 'Chiamaci',
    'titolo_indirizzo' => 'Indirizzo',
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'Azienda',
    'assistenza' => 'Assistenza',
    'prodotti' => 'Prodotti',
    'contatti' => 'Contatti',
    'carrello' => 'Carrello quotazioni',
    
    
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */

    'welcome_titolo' => 'Benvenuti alla Tecno Machine',
    'welcome_titolo_breve' => 'Benvenuti alla Tecno Machine',
    'welcome_testo' => '<p>La Tecno Machine Srl è situata a Manzano in provincia di Udine, e opera da due anni circa nel settore della vendita, riparazione e manutenzione di macchine per la lavorazione del legno.
Lo staff della Tecno Machine può comunque vantare un&#39;esperienza pluridecennale nell&#39;attività di assistenza, vendita e servizio di post-vendita e customer care.
La nostra azienda opera su tutto il territorio italiano, e anche in paesi stranieri, quali ad esempio, Albania, Romania, Slovenia e Vietnam, esportando macchine per la lavorazione del legno, effettuando i relativi collegamenti e manutenzioni se necessarie.</p>',
    'welcome_cta' => 'Benvenuti alla Tecno Machine',
    'assistenza_titolo' => 'Servizio di assistenza',
    'assistenza_titolo_breve' => 'Benvenuti alla Tecno Machine',
    'assistenza_testo' => '<p>La TECNO MACHINE opera da 40 anni nel settore delle macchine per la lavorazione del legno. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
    <p>Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.</p>',
    'claim' => 'La soluzione ad ogni problema',
    'assistenza_claim' => 'La soluzione ad ogni problema',
    'vai' => 'Vai',
    'leggi_tutto' => 'Leggi tutto',
    'inevidenza' => 'Prodotti in evidenza',

    
    /*
    
   |--------------------------------------------------------------------------
   | Prodotti in evidenza
   |--------------------------------------------------------------------------
   */

    'title_prodotti_evidenza' => 'Prodotti in evidenza',
    'apri_scheda' => 'Apri la scheda',
    
    /*
   |--------------------------------------------------------------------------
   | News
   |--------------------------------------------------------------------------
   */

    'titolo_sezione_news' => 'Le nostre ultime News',
    'apri_scheda' => 'APRI LA SCHEDA',
    
    /*
   
   
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'footer_testo_breve' => 'La TECNO MACHINE opera da 40 anni nel settore delle macchine per la lavorazione del legno. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
    'footer_titolo_links' => 'Collegamenti rapidi',
    'footer_titolo_contatti' => 'Contattaci',
    'footer_italia' => 'ITALIA',
    
    'telefonateci' => 'Telefonateci',
    'assistenza_home' => 'Assistenza',
    'nostri_servizi' => 'I nostri servizi',
    
    'telefono' => 'Telefono',
    'indirizzo' => 'Indirizzo',
    'copyright' => 'Copyright',
    'notelegali' => 'Note legali',
    'privacy' => 'Privacy Policy',
       
   

];