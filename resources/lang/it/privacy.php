<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'header_privacy'     => 'Assistenza tecnica',
    'titolo_privacy'     => 'Assistenza tecnica - servizio post vendita',
    'testo_assitenza_primary' => 'Tecno Machine opera da 30 anni sul mercato dei macchinari per la lavorazione del legno. PROSEGUIRE TESTO..... Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.',
         
   

];