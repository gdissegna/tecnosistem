<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_codice'     => 'Codice:',
    'titolo_descrizione'     => 'Descrizione - Caratteristiche tecniche',
    'titolo_img_aggiuntive' => 'Galleria immagini',
    'titolo_video'     => 'Guarda il video',
    'titolo_pdf'     => 'Apri la scheda PDF',
    'carrello_button' => 'Aggiungi al carrello',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
   

];