<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'contatti_title' => 'Contattateci',
    'contatti_form_title' => 'Richiesta informazioni',
    'contatti_form_header' => 'Per ogni informazione a cui sei interessato, compila il modulo che vedi qui riportato: il nostro Staff ti contatterà per fornirti ogni indicazione                                       necessaria. I campi contrassegnati con un asterisco *, sono obbligatori. ',
    'contatti_subtitle' => 'Scriveteci, telefonateci: il nostro Staff per Voi',
    'contatti_indirizzo' => 'Indirizzo',
    'contatti_contatti' => 'Contatti',
    'label_nome' => 'Nome e cognome',
    'label_email' => 'Email',
    'label_societa' => 'Società',
    'label_indirizzo' => 'Indirizzo',
    'label_citta' => 'Citta',
    'label_paese' => 'Paese',
    'label_telefono' => 'Telefono',
    'label_fax' => 'Fax',
    'label_attivita' => 'Tipo di attività',
    'label_rivenditore' => 'Rivenditore',
    'label_utilizzatore' => 'Utilizzatore',
    'label_costruttore' => 'Costruttore',
    'label_messaggio' => 'Messaggio',
    'label_privacy' => 'Privacy',
    'label_privacy_testo' => 'In riferimento al D.lgs 30 giugno 2003, n. 196 (codice in materia di protezione dei dati personali), Le comunichiamo che il trattamento dei suoi dati                                   personali è finalizzato alla gestione di questa operazione, all\'invio di nostro materiale informativo e all\'utilizzo dei dati da parte di Tecnomec                                     S.r.l. Il Titolare del trattamento dei dati è Tecnomec S.r.l. Lei può esercitare i diritti di cui all\'Art. 7 del D.lgs 196/03 (tra cui i diritti di                                     accesso, rettifica, aggiornamento e cancellazione) rivolgendosi a Tecnomec S.r.l., P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Tel.                               +39.0481.676679, Fax +39.0481.676681, E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'Do il mio consenso al trattamento dei dati per i fini sopra descritti. *',
    'label_invia' => 'Invia richiesta',
         
   

];