<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'La nostra Azienda',
    'titolo'     => 'Benvenuti alla Tecno Machine',
    'sottotitolo_azienda'     => 'Vendita e assistenza di macchine per la lavorazione del legno',
    'testo_azienda' => '<p>Tecno Machine opera da 30 anni sul mercato dei macchinari per la lavorazione del legno. PROSEGUIRE TESTO..... Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.</p>
    <p>PROSEGUIRE TESTO..... Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.</p>
    <p>PROSEGUIRE TESTO..... Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione.
    </p>
    ',
    'button_cta'     => 'I nostri servizi',
   

];