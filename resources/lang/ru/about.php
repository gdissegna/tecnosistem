<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'О ПРЕДПРИЯТИИ',
    'titolo'     => 'Tecnomec Srl - ИТАЛЬЯНСКИЙ ЛИДЕР ПО ИЗГОТОВЛЕНИЮ ИНСТРУМЕНТОВ ДЛЯ ДЕРЕВООБРАБОТКИ.',
    'testo' => 'Tecnomec Srl, operating since 1979 on woodworking market, stands nowadays as one of the top-quality labels in its activity. For our company, moved on January 2004 in a                   brand new factory loft (2200 mq), are actually working 15 people, producing HW/HS/DP tools of all kinds. Our technical office, powered by highly experienced designers                   and developers, can easily solve every manufacturing problem and always suggest the correct way of employ, giving to our customers a brilliant assistance service. Raw                   material is the best available on market; moreover, our craftsmen’s experience gives the highest quality level to our products, sold all around the world by a selected                   network of dealers and resellers.',
         
   

];