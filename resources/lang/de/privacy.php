<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'privacy_titolo'     => 'PRIVACY',
    'privacy_subtitle' => 'Gesetz zum Schutz personenbezogener Daten',
    'privacy_testo' => '<p>Tecnomec freut sich, Sie auf Ihrer Webseite zu begrüssen. Bevor Sie unsere Internetseite weiter besuchen, sind Sie eingeladen nachfolgende Informationen sorgfältig zu lesen.</p>
                        <p>GESETZ ZUM SCHUTZ PERSONENBEZOGENER DATEN</p>
                        <p>Mit Bezug auf die italienische Gesetzverordnung 196/2003 (Schutzmaßnahme der Personalangaben) teilen wir Ihnen mit, dass Ihre Personalangaben nur zwecks Internetdienst (Besuch der Webseite, Informationsanfrage, Registrierung für Dienstleistungen) und zwecks Zusendung unserer Verkaufsunterlagen dienen.</p>
                        <p>Die Ausübung dieser Tätigkeit ist für die Erfüllung Ihrer Anfrage notwendig.</p>
                        <p>IL TITOLARE DEL TRATTAMENTO DEI DATI È TECNOMEC S.r.l.</p>
                        <p>Sie können Ihre laut Art.7 der italienischen Gesetzverordnung 196/2003 vorgesehenen Rechte ausüben indem Sie sich wenden an::</p>
                        <p>TECNOMEC S.r.l.<br />
                        Via Aquileia, 54 (SS305)<br />
                        34071 Cormons (GO)<br />
                        Italien</p>',     
   

];