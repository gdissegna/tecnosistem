@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>Blog Details</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li><a href="{{ route('newslist') }}">News</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->

    <div class="page-wrapper section-space--inner--120">
        <!--Blog section start-->
        <div class="blog-section">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12">
                        <div class="row">

                            <div class="blog-details col-12">
                                <div class="blog-inner">
                                    <div class="media">
                                        <div class="image"><img src="{{ asset($article->primary_img) }}" alt=""></div>
                                    </div>
                                    <div class="content">
                                        <ul class="meta">
                                            <li>{{ $article->updated_at->format('d/m/Y') }}</li>
                                        </ul>
                                        <h2 class="title">{{ $article->title }}</h2>
                                        <div class="desc section-space--bottom--30">
                                           {!! $article->body !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection