<div class="large-3 medium-4 columns">
    <div class="hide-for-small">
        <div class="sidebar">
            <section>
                <h5>Menu Utenti</h5>
                <nav>
                    <ul class="side-nav">
                        <li><a href="">Users</a></li>
                        <li><a href="{{ url('auth/adduser') }}">Add User</a></li>
                    </ul>
                </nav>
            </section>
            <hr>
            <section>
                <h5>Menu Cms</h5>
                <nav>
                    <ul class="side-nav">
                        <li><a href="/admin">slider</a></li>
                        <li><a href="{{ route('articles') }}">News</a></li>
                        <li><a href="{{ route('products') }}">Prodotti</a></li>
                        <li><a href="{{ route('categories') }}">Categorie :tmp</a></li>
                        <li><a href="{{ route('Offers') }}">Offerte</a></li>
                    </ul>
                </nav>
            </section>
        </div>
    </div>
</div>

