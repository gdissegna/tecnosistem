@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1>Lista news</h1>
        </div>
        <div class="large-4 medium-4 columns"> <a role="button" href="{{ route('ArticleCreate') }}" class="button right">Crea News</a></div>
        <hr>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>Titolo</th>
                <th>Creata il</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                <tr>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td><ul class="button-group">
                            <li><a href="{{ route('ArticleEdit',['id' => $article->id ]) }}" class="button small">Edita</a></li>
                            <li><a href="{{ route('ArticleDelete',['id' => $article->id ]) }}" class="button small">Cancella</a></li>
                        </ul>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

