@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <h1>Edita L'articolo</h1>
        <hr>
        <div class="row">
            <div class="large-9 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            {!! Form::open(['route' => array('ArticleEditPost', $article->id), 'method' => 'post', 'files' => true]) !!}
            <div class="large-12">
                <label>Titolo IT
                    {!! Form::text('title_it', $article->translate('it')->title) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Titolo EN
                    {!! Form::text('title_en', $article->translate('en')->title) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Corpo IT
                    {!! Form::textarea('body_it', $article->translate('it')->body,['id' => 'editor']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Corpo EN
                    {!! Form::textarea('body_en', $article->translate('en')->body,['id' => 'editor']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Inserisci Immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}
            <br>
            <br>
            <br>

        </div>
    </div>
@endsection