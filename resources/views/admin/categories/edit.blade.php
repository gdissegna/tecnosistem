@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">

            <h1>Inserisci nuovo articolo</h1>
            <hr>
        </div>
        <div class="row">
            <div class="large-12 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['route' => ['CategoryEditPost', $category->id ], 'method' => 'post', 'files' => true]) !!}
            <div class="large-12 columns">
                <label>Nome IT
                    {!! Form::text('name_it', $category->translate('it')->name) !!}
                </label>
            </div>
            <div class="large-12 columns">
                <label>Nome EN
                    {!! Form::text('name_en', $category->translate('en')->name) !!}
                </label>
            </div>
            <div class="large-12 columns">
                <label>Descrizione IT
                    {!! Form::textarea('description_it', $category->translate('it')->description) !!}
                </label>
            </div>
            <div class="large-12 columns">
                <label>Descrizione EN
                    {!! Form::textarea('description_en', $category->translate('en')->description) !!}
                </label>
            </div>
            <div class="large-12 columns">
                <label>Inserisci immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}
    </div>
@endsection