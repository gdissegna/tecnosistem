@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1 class="">Lista Categorie</h1>
        </div>
        <div class="large-4 medium-4 columns"><a role="button" href="{{ route('CategoryCreate') }}" class="button right">Aggiungi categoria</a></div>
        <hr>
        <p></p>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>nome categoria</th>
                <th width="200">aggiunta il:</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>{{ $category->created_at }}</td>
                <td><ul class="button-group">
                        <li><a href="{{ route('CategoryEdit', ['id' => $category->id ]) }}" class="small button">edita</a></li>
                        <li><a href="{{ route('CategoryDelete',[ 'id' => $category->id ]) }}" class="small button">Cancella</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection

{{ route('CategoryCreate') }}