@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <h1>Crea una nuova offerta</h1>
        <hr>
        <div class="row">
            <div class="large-9 columns">
                @if (count($errors) > 0)
                    <div class="alert-box alert radius">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            {!! Form::open(['route' => 'OfferCreatePost', 'method' => 'post', 'files' => 'true']) !!}
            <div class="large-12">
                <label>Titolo IT
                    {!! Form::text('title_it', '') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Titolo EN
                    {!! Form::text('title_en', '') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione IT
                    {!! Form::text('description_it', '') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione EN
                    {!! Form::text('description_en', '') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Carica immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Carica Pdf
                    {!! Form::file('pdf') !!}
                </label>
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}


        </div>
    </div>
@endsection