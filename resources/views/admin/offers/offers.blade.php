@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1>Lista Offerte</h1>
        </div>
        <div class="large-4 medium-4 columns"> <a role="button" href="{{ route('OfferCreate') }}" class="button right">Crea Offerta</a></div>
        <hr>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>Titolo</th>
                <th>Creata il</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
                @foreach($offers as $offer)
                <tr>
                    <td>{{ $offer->title }}</td>
                    <td>{{ $offer->created_at }}</td>
                    <td><ul class="button-group">
                            <li><a href="{{ route('OfferEdit',[$offer->id]) }}" class="button small">Edita</a></li>
                            <li><a href="{{ route('OfferDelete',[$offer->id]) }}" class="button small">Cancella</a></li>
                        </ul>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endsection

