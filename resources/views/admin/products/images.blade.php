@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="large-8 medium-8 columns">
            <h1 class="">Lista immagini prodotto</h1>
        </div>
        <div class="large-4 medium-4 columns"><a role="button" href="{{ route('ProductImagesCreate', ['id' => $product->id ]) }}" class="button right">Aggiungi immagine</a></div>
        <hr>
        <p></p>
        <table class="large-12 medium-12">
            <thead>
            <tr>
                <th>immagine</th>
                <th width="200">aggiunto il:</th>
                <th width="200">opzioni</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product->images as $image)
            <tr>
                <td><img src="{{ asset($image->image) }}" width="300" alt="Recent Work"
                         class="img-responsive"/></td>

                <td>{{ $image->created_at }}</td>
                <td><ul class="button-group">
                        <li><a href="{{ route('ProductImagesEdit',[$image->product_id, $image->id]) }}" class="button small">edita</a></li>
                        <li><a href="{{ route('ProductImagesDelete',[$image->product_id, $image->id]) }}" class="button small">Cancella</a></li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection