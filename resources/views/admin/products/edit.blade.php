@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="row">
            <h1>Edita il Prodotto</h1>
            <hr>
            <div class="large-12">
                @if (count($errors) > 0)
                    <div>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><div class="alert-box alert" data-alert=""> {{ $error }}</div></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            {!! Form::open(['route' => ['ProductEditPost', $product->id ], 'method' => 'post', 'files' => 'true']) !!}
            <div class="large-12">
                <label>Nome prodotto IT
                    {!! Form::text('name_it', $product->translate('it')->name) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Nome prodotto EN
                    {!! Form::text('name_en', $product->translate('en')->name) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Codice Articolo
                    {!! Form::text('codice', $product->codice) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione IT
                    {!! Form::textarea('description_it', $product->translate('it')->description,['id' => 'tinymce']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione EN
                    {!! Form::textarea('description_en', $product->translate('en')->description,['id' => 'tinymce']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Seleziona una Categoria
                    <select name="category">
                        @foreach($categories as $category)
                            @if($product->category_id === $category->id )
                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                            @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endif
                          @endforeach
                    </select>
                </label>
            </div>
            <div class="large-12">
                <label>Edita il frame Video
                    {!! Form::text('video', $product->video) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Modifica immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Modifica file Pdf
                    {!! Form::file('pdf') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Spunta se in evidenza</label>
                {!! Form::checkbox('inhome') !!}
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>

            {!! Form::close() !!}


        </div>
    </div>
@endsection