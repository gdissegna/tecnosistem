@extends('admin.master')

@section('content')
    <div class="large-9 medium-8 columns">
        <div class="row">

            <h1>Crea Nuovo Prodotto</h1>
            <hr>
            <div class="large-12">
            @if (count($errors) > 0)
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><div class="alert-box alert" data-alert=""> {{ $error }}</div></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            </div>
            {!! Form::open(['route' => 'ProductCreatePost', 'method' => 'post', 'files' => 'true']) !!}
            <div class="large-12">
                <label>Nome prodotto IT
                    {!! Form::text('name_it', '',['placeholder' => 'nome it']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Nome prodotto EN
                    {!! Form::text('name_en', '',['placeholder' => 'nome en']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Codice Articolo
                    {!! Form::text('codice', '',['placeholder' => 'Codice articolo']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione IT
                    {!! Form::textarea('description_it', 'Description_it',['id' => 'tinymce']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Descrizione EN
                    {!! Form::textarea('description_en', 'Description_en',['id' => 'tinymce']) !!}
                </label>
            </div>
            <div class="large-12">
                <label>Selexziona una Categoria
                    <select name="category">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="large-12">
                <label>Inserisci frame Video
                    {!! Form::text('video', 'Video') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Inserisci immagine
                    {!! Form::file('image') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Aggiungi file Pdf
                    {!! Form::file('pdf') !!}
                </label>
            </div>
            <div class="large-12">
                <label>Spunta se in evidenza</label>
                {!! Form::checkbox('inhome') !!}
            </div>
            <div class="large-12">
                {!! Form::submit('Submit', ['class' => 'button']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection