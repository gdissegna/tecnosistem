@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>Contact Us</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li>Contact Us</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--120">
        <!--Contact section start-->
        <div class="conact-section">
            <div class="container">

                <div class="row">
                    <div class="col-lg-4 col-12">
                        <div class="contact-information">
                            <h3>Contact Information</h3>
                            <ul>
                                <li>
                                    <span class="icon"><i class="ion-android-map"></i></span>
                                    <span class="text"><span>Stock Building, 125 Main Street 1st Lane, San Francisco, USA</span></span>
                                </li>
                                <li>
                                    <span class="icon"><i class="ion-ios-telephone-outline"></i></span>
                                    <span class="text"><a href="#">(001) 24568 365 987)</a><a href="#">(001) 65897 569 784)</a></span>
                                </li>
                                <li>
                                    <span class="icon"><i class="ion-ios-email-outline"></i></span>
                                    <span class="text"><a href="#">infor@example.com</a><a href="#">www.example.com</a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="project-item-wrapper">
                            @foreach($cartitems as $product)
                                <div class="col-lg-4 col-sm-6 col-12 section-space--bottom--30">
                                    <div class="service-grid-item service-grid-item--style2">
                                        <div class="service-grid-item__image">
                                            <div class="service-grid-item__image-wrapper">
                                                <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}">
                                                    <img src="{{ asset($product->primary_img) }}" class="img-fluid" alt="{{ $product->name }}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="service-grid-item__content">
                                            <h3 class="title">
                                                <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}">{{ $product->name }}g</a>
                                            </h3>
                                            <h4>{{ $product->codice }}</h4>
                                            <p class="subtitle">{!! str_limit($product->description,$limit = 130,$end = '...' ) !!}</p>
                                            <a href="{{ route('carrelloRemoveItem',[$product->id]) }}"
                                               class="read-more">{{ trans('custom.rimuovi_carrello') }}</a>
                                            <a href="{{ route('prodotto',[$product->category->slug , $product->slug]) }}"
                                               class="read-more">{{ trans('custom.leggi_tutto') }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <div class="contact-form">
                            <h3>{{ trans('custom.contatti_form_title') }}</h3>
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert-box alert radius">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form id="contact-form" action="{{ route('carrellopost') }}" method="post">
                                <div class="row row-10">
                                    <input class="hidden" name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="con_name" type="text" placeholder="Your Name"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="con_email" type="email" placeholder="Your Email"></div>
                                    <div class="col-12"><textarea name="con_message" placeholder="Your Message"></textarea></div>
                                    <div class="col-12"><button>Send Message</button></div>
                                </div>
                            </form>
                            <p class="form-message">{{ trans('custom.contatti_form_header') }}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Contact section end-->
    </div>

@endsection
