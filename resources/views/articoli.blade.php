@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>Blog</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li>Blog</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--120">
        <div class="blog-section">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12">
                        <div class="row">
                            @foreach($articles as $article)
                            <div class="col-sm-6 col-12">
                                <div class="blog-post-slider__single-slide blog-post-slider__single-slide--grid-view">
                                    <div class="blog-post-slider__image section-space--bottom--30">
                                        <a href="blog-details-left-sidebar.html"><img src="assets/img/blog/1.jpg" class="img-fluid" alt=""></a>
                                    </div>
                                    <div class="blog-post-slider__content">
                                        <p class="post-date">{{ $article->updated_at->format('F j, Y') }}/p>
                                        <h3 class="post-title">
                                            <a href="blog-details-left-sidebar.html">{{ $article->title }}</a>
                                        </h3>
                                        <p class="post-excerpt">{{ $article->body }}</p>
                                        <a href="{{  route('singlenews',[ $article->slug]) }}" class="see-more-link">SEE MORE</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="row ">
                            <div class="col">
                                <ul class="page-pagination section-space--top--30">
                                    <li><a href="#"><i class="fa fa-angle-left"></i> Prev</a></li>
                                    <li class="active"><a href="#">01</a></li>
                                    <li><a href="#">02</a></li>
                                    <li><a href="#">03</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i> Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>

@endsection

