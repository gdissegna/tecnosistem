// MENU

<li class="has-children has-children--multilevel-submenu">
    <a href="">{{ trans('custom.prodotti') }}</a>
    <ul class="submenu">
        @foreach($menu_categories as $menu_category)
            <li><a href="{{ route('categoria',[$menu_category->slug]) }}" title="{{ $menu_category->name }}">{{ $menu_category->name }}</a></li>
        @endforeach
    </ul>
</li>

<li> <a href="{{ route('carrello') }}">{{ trans('custom.carrello') }}</a></li>

// HOME Product

<a href="{{  route('prodotto',[ $product->category->slug , $product->slug]) }}" title="{{ $product->name }}"  class="see-more-link">{{ trans('custom.apri_scheda') }}</a>

// FOOTER

<li class="menu-item-has-children">
    {{ trans('custom.prodotti') }}
    <ul class="sub-menu">
        @foreach($menu_categories as $menu_category)
            <li><a href="{{ route('categoria',[$menu_category->slug]) }}" title="{{ $menu_category->name }}">{{ $menu_category->name }}</a></li>
        @endforeach
    </ul>
</li>