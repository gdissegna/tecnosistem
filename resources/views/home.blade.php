@extends('master')

@section('content')
    <!--====================  hero slider area ====================-->
    <div class="hero-alider-area">
        <div class="hero-slider__container-area">
            <div class="swiper-container hero-slider__container">
                <div class="swiper-wrapper hero-slider__wrapper">
                    @foreach($sliders as $slider)
                    <!--=======  single slider item  =======-->
                    <div class="swiper-slide hero-slider__single-item bg-img" data-bg="{{ $slider->path }}">
                        <div class="hero-slider__content-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="hero-slider__content">
                                            <h2 class="hero-slider__title">Benvenuti alla Tecno Machine</h2>
                                            <p class="hero-slider__text">Vendita e assistenza di macchine per la lavorazione del legno</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!--=======  End of single slider item  =======-->
                </div>
            </div>
            <div class="ht-swiper-button-prev ht-swiper-button-prev-13 ht-swiper-button-nav d-none d-xl-block"><i class="ion-ios-arrow-left"></i></div>
            <div class="ht-swiper-button-next ht-swiper-button-next-13 ht-swiper-button-nav d-none d-xl-block"><i class="ion-ios-arrow-forward"></i></div>
        </div>
    </div>
    <!--====================  End of hero slider area  ====================-->
 <!--====================  video cta area ====================-->
    <div class="video-cta-area section-space--inner--120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="video-cta-content">
                        <h4 class="video-cta-content__small-title">{{ trans('custom.welcome_titolo') }}</h4>
                        <h3 class="video-cta-content__title">{{ trans('custom.welcome_titolo_breve') }}</h3>
                        <p class="video-cta-content__text">{!! trans('custom.welcome_testo') !!}</p>
                        <a href="" class="ht-btn ht-btn--round">{{ trans('custom.welcome_cta') }}</a>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1 col-md-12">
                    <div class="cta-video-image">
                            <a href="">
                                <div class="cta-video-image__image">
                                    <img src="{{ asset('images/tecnomachine-home-welcome.jpg') }}" class="img-fluid" alt="">
                                </div>
                                
                            </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of video cta area  ====================-->
 <!--====================  video cta area ====================-->
    <div class="video-cta-area section-space--inner--120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="cta-video-image">
                            <a href="">
                                <div class="cta-video-image__image">
                                    <img src="{{ asset('images/tecnomachine-home-welcome.jpg') }}" class="img-fluid" alt="">
                                </div>
                                
                            </a>
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-12">
                    <div class="video-cta-content">
                        <h4 class="video-cta-content__small-title">{{ trans('custom.assistenza_titolo') }}</h4>
                        <h3 class="video-cta-content__title">{{ trans('custom.assistenza_titolo_breve') }}</h3>
                        <p class="video-cta-content__text">{!! trans('custom.assistenza_testo') !!}</p>
                        <a href="" class="ht-btn ht-btn--round">{{ trans('custom.assistenza_cta') }}</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!--====================  End of video cta area  ====================-->
    <!--====================  project grid slider area ====================-->

    <div class="service-slider-title-area grey-bg section-space--inner--top--120 section-space--inner--bottom--285">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-area text-center">
                        <h2 class="section-title mb-0">{{ trans('custom.title_prodotti_evidenza') }} <span class="title-icon"></span></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="service-grid-slider-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="service-slider">
                        <div class="swiper-container service-slider__container service-slider__container--style2">
                            <div class="swiper-wrapper service-slider__wrapper">
                                @foreach($products as $product)
                                <div class="swiper-slide">
                                    <div class="service-grid-item service-grid-item--style2">
                                        <div class="service-grid-item__image">
                                            <div class="service-grid-item__image-wrapper">
                                                <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}">
                                                    <img src="{{ asset($product->primary_img) }}" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="service-grid-item__content">
                                            <h3 class="title">
                                                <a href="{{ route('prodotto',[ $product->category->slug , $product->slug]) }}">{{ $product->name }}</a>
                                            </h3>
                                            <h4>Cod.{{ $product->codice }}</h4>
                                            <p class="subtitle">{!! $product->description !!}</p>
                                          
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="ht-swiper-button-prev ht-swiper-button-prev-4 ht-swiper-button-nav"><i class="ion-ios-arrow-left"></i></div>
                        <div class="ht-swiper-button-next ht-swiper-button-next-4 ht-swiper-button-nav"><i class="ion-ios-arrow-forward"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project grid slider area  ====================-->
    <!--====================  blog grid area ====================-->
    <div class="blog-grid-area section-space--inner--120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title-area text-center">
                        <h2 class="section-title section-space--bottom--50">{{ trans('custom.titolo_sezione_news') }}<span class="title-icon"></span></h2>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="blog-grid-wrapper">
                        <div class="row">
                            @foreach($articles as $article)
                            <div class="col-lg-4">
                                <div class="blog-post-slider__single-slide blog-post-slider__single-slide--grid-view">
                                    <div class="blog-post-slider__image section-space--bottom--30">
                                        <a href="{{  route('singlenews',[ $article->slug]) }}"><img src=" {{ asset($article->primary_img) }} " class="img-fluid" alt=""></a>
                                    </div>
                                    <div class="blog-post-slider__content">
                                        <p class="post-date"> {{ $article->updated_at->format('d/m/Y') }}</p>
                                        <h3 class="post-title">
                                            <a href="blog-details-left-sidebar.html">{{ $article->title }}</a>
                                        </h3>
                                        <p class="post-excerpt">{!! str_limit($article->body,$limit = 50,$end = '...' ) !!}</p>
                                        <a href="{{  route('singlenews',[ $article->slug]) }}" class="see-more-link">SEE MORE</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of blog grid area  ====================-->

@endsection()
