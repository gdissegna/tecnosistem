<!DOCTYPE html "-//w3c//dtd xhtml 1.0 transitional //en" "http://www.w3.org/tr/xhtml1/dtd/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!--[if gte mso 9]><xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">

    <title>Tecno Machine S.r.l.</title>

</head>

<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% !important;margin: 0;padding: 0;background-color: #FFFFFF">
<style id="media-query">
    /*  Media Queries */

    @media only screen and (max-width: 500px) {
        table[class="body"] img {
            height: auto !important;
            max-width: 100% !important;
        }
        table[class="body"] img.fullwidth {
            width: 100% !important;
        }
        table[class="body"] center {
            min-width: 0 !important;
        }
        table[class="body"] .container {
            width: 95% !important;
        }
        table[class="body"] .row {
            width: 100% !important;
            display: block !important;
        }
        table[class="body"] .wrapper {
            display: block !important;
            padding-right: 0 !important;
        }
        table[class="body"] .columns,
        table[class="body"] .column {
            table-layout: fixed !important;
            float: none !important;
            width: 100% !important;
            padding-right: 0px !important;
            padding-left: 0px !important;
            display: block !important;
        }
        table[class="body"] .wrapper.first .columns,
        table[class="body"] .wrapper.first .column {
            display: table !important;
        }
        table[class="body"] table.columns td,
        table[class="body"] table.column td,
        .col {
            width: 100% !important;
        }
        table[class="body"] table.columns td.expander {
            width: 1px !important;
        }
        table[class="body"] .right-text-pad,
        table[class="body"] .text-pad-right {
            padding-left: 10px !important;
        }
        table[class="body"] .left-text-pad,
        table[class="body"] .text-pad-left {
            padding-right: 10px !important;
        }
        table[class="body"] .hide-for-small,
        table[class="body"] .show-for-desktop {
            display: none !important;
        }
        table[class="body"] .show-for-small,
        table[class="body"] .hide-for-desktop {
            display: inherit !important;
        }
        .mixed-two-up .col {
            width: 100% !important;
        }
    }

    @media screen and (max-width: 500px) {
        div[class="col"] {
            width: 100% !important;
        }
    }

    @media screen and (min-width: 501px) {
        table[class="block-grid"] {
            width: 500px !important;
        }
    }
</style>
<table class="body" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;height: 100%;width: 100%;table-layout: fixed" cellpadding="0" cellspacing="0" width="100%" border="0">
    <tbody>
    <tr style="vertical-align: top">
        <td class="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;background-color: #FFFFFF" align="center" valign="top">
            <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: #FFFFFF" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                <tbody>
                <tr style="vertical-align: top">
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width='500' class="ieCell" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                        <![endif]-->
                        <table class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                            <tbody>
                            <tr style="vertical-align: top">
                                <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                                    <table class="block-grid" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: transparent" cellpadding="0" cellspacing="0" width="100%" bgcolor="transparent">
                                        <tbody>
                                        <tr style="vertical-align: top">
                                            <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;font-size: 0">
                                                <!--[if (gte mso 9)|(IE)]>
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td valign="top">
                                                <![endif]-->

                                                <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                                    <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                        <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 2px solid transparent;border-left: 0px solid transparent">
                                                                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%" border="0">
                                                                    <tbody>
                                                                    <tr style="vertical-align: top">
                                                                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;width: 100%;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px" align="center">
                                                                            <div align="center">
                                                                                <a href="http://www.tecnomec.it" target="_blank">
                                                                                    <img class="center fullwidth" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block;border: none;height: auto;line-height: 100%;margin: 0 auto;float: none;width: 100% !important;max-width: 480px !important" align="center" border="0" data-custom-width="480" src="{{ asset('/images/logo-tecnomachine-form.png') }}" alt="Image" title="Image">
                                                                                </a>

                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
            <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top;background-color: transparent" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                <tbody>
                <tr style="vertical-align: top">
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width='500' class="ieCell" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                        <![endif]-->
                        <table class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                            <tbody>
                            <tr style="vertical-align: top">
                                <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top" width="100%">
                                    <table class="block-grid" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #333;background-color: transparent" cellpadding="0" cellspacing="0" width="100%" bgcolor="transparent">
                                        <tbody>
                                        <tr style="vertical-align: top">
                                            <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;text-align: center;font-size: 0">
                                                <!--[if (gte mso 9)|(IE)]>
                                                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td valign="top">
                                                <![endif]-->

                                                <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                                    <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" align="center" width="100%" border="0">
                                                        <tbody>
                                                        <tr style="vertical-align: top">
                                                            <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 30px;padding-right: 0px;padding-bottom: 30px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
                                                                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                    <tr style="vertical-align: top">
                                                                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px">
                                                                            <div style="color:#555555;line-height:120%;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;">
                                                                                <div style="font-size:14px;line-height:17px;color:#555555;font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;text-align:left;"><span style="font-size:24px; line-height:29px;"><strong><span style="font-family: arial, helvetica, sans-serif; line-height: 28px; font-size: 24px;" data-mce-style="font-family: arial, helvetica, sans-serif; line-height: 28px; font-size: 24px;"><span style="font-size: 20px; line-height: 24px;" data-mce-style="font-size: 20px;">Ecco i dati compilati nel form</span>
                                                                                                                <br>
                                                                                                                </span>
                                                                                        </strong>
                                                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table style="border-spacing: 0;border-collapse: collapse;vertical-align: top" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                    <tr style="vertical-align: top">
                                                                        <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important;vertical-align: top;padding-top: 15px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                            <div style="color:#aaaaaa;line-height:200%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">nome e cognome: {{ $request['name'] }} {{ $request['cognome'] }}
                                                                                    <br>
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">societ&#224;: {{ $request['societa'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">indirizzo: {{ $request['indirizzo'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Citt&#224;: {{ $request['citta'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Paese: {{ $request['paese'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Telefono: {{ $request['phone'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Fax: {{ $request['fax'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Email: {{ $request['email'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Attivita: {{ $request['attivita'] }}
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">Messaggio:
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                                <div style="font-size:14px;line-height:28px;color:#aaaaaa;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                                    <p>
                                                                                        {!!  $request['message'] !!}
                                                                                    </p>
                                                                                    <br data-mce-bogus="1">
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</body>

</html>