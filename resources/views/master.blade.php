<!DOCTYPE html>
<html class="no-js" lang="{{ LaravelLocalization::getCurrentLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- SEO -->
    <title></title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="copyright" content="">
    <meta name="application-name" content="">
    <!--GEO Tags-->
    <meta name="DC.title" content="@yield('title', config('app.name'))"/>
    <meta name="geo.region" content="GB-HMF"/>
    <meta name="geo.placename" content="London"/>
    <meta name="geo.position" content="51.493272;-0.239747"/>
    <meta name="ICBM" content="51.493272, -0.239747"/>
    <!--Facebook Tags-->
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{ request()->fullUrl() }}"/>
    <meta property="og:title" content="@yield('title', config('app.name'))"/>
    <meta property="og:description" content="@yield('description', config('app.description'))"/>
    <meta property="og:image" content="{{ asset('')  }}"/>
    <meta property="article:author" content="https://www.facebook.com/TODO"/>
    <meta property="og:locale" content="{{ Config::get('app.locale') }}"/>
    
    <!-- device width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <!-- Csrf Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('partials.css')
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146487292-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-146487292-1');
    </script>
</head>

<body>
@include('partials.header')

@yield('content')

<!--====================  End of blog grid area  ====================-->
<!--====================  footer area ====================-->
<div class="footer-area dark-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-content-wrapper section-space--inner--100">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-12">
                            <!-- footer intro wrapper -->
                            <div class="footer-intro-wrapper">
                                <div class="footer-logo">
                                    <a href="#">
                                        <img src="{{ asset('images/logo-tecnomachine-footer.png') }}" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="footer-desc">
                                    {{ trans('custom.footer_testo_breve') }}
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-2  col-lg-3 col-md-2">
                            <!-- footer widget -->
                            <div class="footer-widget">
                                <h4 class="footer-widget__title">{{ trans('custom.footer_titolo_links') }}</h4>
                                <ul class="footer-widget__navigation">
                                    <li><a href="{{ route('index') }}">{{ trans('custom.home') }}</a></li>
                                    <li><a href="{{ route('azienda') }}">{{ trans('custom.azienda') }}</a></li>
                                    <li><a href="{{ route('assistenza') }}">{{ trans('custom.assistenza') }}</a></li>
                                    <li><a href="{{ route('newslist') }}">News</a></li>
                                    <li><a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a></li>
                                    <li><a href="{{ route('carrello') }}">{{ trans('custom.carello') }}</a></li>
                                    <li><a href="{{ route('privacy') }}">Privacy policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-12">
                            <!-- footer widget -->
                            <div class="footer-widget">
                                <h4 class="footer-widget__title">Newsletter</h4>
                                @if(LaravelLocalization::getCurrentLocale() == 'it')
                                    <form id="contact-form" class="contact-form" action="{{ route('newsletterpost') }}" method="post">
                                        {!! csrf_field() !!}
                                        <input class="hidden" name="listID" type="hidden" value="b55cb1380a">
                                        <input class="section-space--bottom--20"  name="name" type="text" placeholder="{{ trans('contatti.label_nome') }}">
                                        <input name="email" type="email" placeholder="{{ trans('contatti.label_email') }}">
                                        <div class="col-12"><button>{{ trans('contatti.label_invia') }}</button></div>
                                    </form>
                                @else
                                    <form id="contact-form" class="contact-form" action="{{ route('newsletterpost') }}" method="post">
                                        {!! csrf_field() !!}
                                        <input class="hidden" name="listID" type="hidden" value="b0d228c8f2">
                                        <input class="section-space--bottom--20" name="name" type="text" placeholder="{{ trans('contatti.label_nome') }}">
                                        <input name="email" type="email" placeholder="{{ trans('contatti.label_email') }}">
                                        <div class="col-12"><button>{{ trans('contatti.label_invia') }}</button></div>
                                    </form>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-2 offset-xl-1 col-lg-3 col-md-12">
                            <!-- footer widget -->
                            <div class="footer-widget mb-0">
                                <h4 class="footer-widget__title">{{ trans('custom.footer_titolo_contatti') }}</h4>
                                <div class="footer-widget__content">
                                    <p class="address">Tecno Machine S.r.l.<br> Via Thonet, 2 - 33044 Manzano (UD) - {{ trans('custom.footer_italia') }}</p>
                                    <ul class="contact-details">
                                        <li><span>{{ trans('custom.telefono') }}:</span>+39.0432.755056</li>
                                        <li><span>E-mail:</span><a href="mailto:info@tecnomachine.it">info@tecnomachine.it</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright-wrapper">
        <div class="footer-copyright text-center">
            Copyright © 2019. All right reserved
        </div>
    </div>
</div>
<!--====================  End of footer area  ====================-->
<!--=======  offcanvas mobile menu  =======-->

<div class="offcanvas-mobile-menu" id="mobile-menu-overlay">
    <a href="javascript:void(0)" class="offcanvas-menu-close" id="mobile-menu-close-trigger">
        <i class="ion-android-close"></i>
    </a>

    <div class="offcanvas-wrapper">

        <div class="offcanvas-inner-content">
            <nav class="offcanvas-navigation">
                <ul>
                    <li class="">
                        <a href="{{ route('index') }}">{{ trans('custom.home') }}</a>
                    </li>
                    <li><a href="{{ route('azienda') }}">{{ trans('custom.azienda') }}</a></li>
                    <li><a href="{{ route('assistenza') }}">{{ trans('custom.assistenza') }}</a></li>

                    <li><a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a></li>
                </ul>
            </nav>

            <div class="offcanvas-widget-area">
                <div class="off-canvas-contact-widget">
                    <div class="header-contact-info">
                        <ul class="header-contact-info__list">
                            <li><i class="ion-android-phone-portrait"></i> <a href="tel://+39.0432.755056">+39.0432.755056</a></li>
                            <li><i class="ion-android-mail"></i> <a href="mailto:info@tecnomachine.it">info@tecnomachine.it</a></li>
                        </ul>
                    </div>
                </div>
                <!--
                <div class="off-canvas-widget-social">
                    <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                    <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                    <a href="#" title="Vimeo"><i class="fa fa-vimeo-square"></i></a>
                </div>
                Off Canvas Widget Social End-->
            </div>
        </div>
    </div>

</div>

<!--=======  End of offcanvas mobile menu  =======-->
<!--====================  scroll top ====================-->
<a href="#" class="scroll-top" id="scroll-top">
    <i class="ion-android-arrow-up"></i>
</a>
<!--====================  End of scroll top  ====================-->
<!-- JS
============================================ -->

@include('partials.js')
</body>