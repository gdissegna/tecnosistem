@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>{{ $category->name }}</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="{{ route('index') }}">{{ trans('custom.index') }}</a></li>
                            <li>{{ trans('custom.prodotti') }}</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--120">
        <!--Projects section start-->
        <div class="project-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="content section-space--top--30">
                            <div class="row">
                                <div class="col-12">
                                    {!! $category->description !!}
                                </div>
                            </div>
                        </div>
                        <div class="project-item-wrapper">
                            @foreach($category->products as $product)
                                <div class="col-lg-4 col-sm-6 col-12 section-space--bottom--30">
                                    <div class="service-grid-item service-grid-item--style2">
                                        <div class="service-grid-item__image">
                                            <div class="service-grid-item__image-wrapper">
                                                <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}">
                                                    <img src="{{ asset($product->primary_img) }}" class="img-fluid" alt="{{ $product->name }}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="service-grid-item__content">
                                            <h3 class="title">
                                                <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}">{{ $product->name }}g</a>
                                            </h3>
                                            <h4>{{ $product->codice }}</h4>
                                            <p class="subtitle">{!! str_limit($product->description,$limit = 130,$end = '...' ) !!}</p>
                                            <a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}" class="see-more-link">{{ trans('custom.apri_scheda') }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--
                <div class="row section-space--top--60">
                    <div class="col">
                        <ul class="page-pagination">
                            <li><a href="#"><i class="fa fa-angle-left"></i> Prev</a></li>
                            <li class="active"><a href="#">01</a></li>
                            <li><a href="#">02</a></li>
                            <li><a href="#">03</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Next</a></li>
                        </ul>
                    </div>
                </div>
                -->

            </div>
        </div>
        <!--Projects section end-->
    </div>
    <!--
    <div class="brand-logo-slider-area grey-bg section-space--inner--60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- brand logo slider
                    <div class="brand-logo-slider__container-area">
                        <div class="swiper-container brand-logo-slider__container">
                            <div class="swiper-wrapper brand-logo-slider__wrapper">
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/1.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/2.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/3.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/4.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/1.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/2.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/3.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>
                                <div class="swiper-slide brand-logo-slider__single">
                                    <div class="image">
                                        <a href="#">
                                            <img src="assets/img/brand-logo/4.png" class="img-fluid" alt="">
                                        </a>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   End of brand logo area  ====================-->

@endsection
