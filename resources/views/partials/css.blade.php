<!-- CSS ============================================ -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.min.css')}}">

<!-- FontAwesome CSS -->
<link rel="stylesheet" href="{{ asset('css/vendor/font-awesome.min.css')}}">

<!-- Material design iconic font CSS -->
<link rel="stylesheet" href="{{ asset('css/vendor/material-design-iconic-font.min.css')}}">

<!-- Ionicons CSS -->
<link rel="stylesheet" href="{{ asset('css/vendor/ionicons.min.css')}}">

<!-- Flaticon CSS -->
<link rel="stylesheet" href="{{ asset('css/vendor/flaticon.min.css')}}">

<!-- Swiper slider CSS -->
<link rel="stylesheet" href="{{ asset('css/plugins/swiper.min.css')}}">

<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('css/plugins/animate.min.css')}}">

<!-- Light gallery CSS -->
<link rel="stylesheet" href="{{ asset('css/plugins/lightgallery.min.css')}}">


<!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->
<!--
<link rel="stylesheet" href="assets/css/vendor/vendor.min.css">
<link rel="stylesheet" href="assets/css/plugins/plugins.min.css">
-->



<!-- Main Style CSS -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">



