<!-- JS ============================================ -->

<!-- Modernizer JS -->
<script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>

<!-- jQuery JS -->
<script src="{{ asset('js/vendor/jquery-3.3.1.min.js') }}"></script>

<!-- Bootstrap JS -->
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

<!-- Popper JS -->
<script src="{{ asset('js/vendor/popper.min.js') }}"></script>

<!-- Swiper Slider JS -->
<script src="{{ asset('js/plugins/swiper.min.js') }}"></script>

<!-- Light gallery JS -->
<script src="{{ asset('js/plugins/lightgallery.min.js') }}"></script>

<!-- Light gallery video JS -->
<script src="{{ asset('js/plugins/lg-video.min.js') }}"></script>

<!-- Waypoints JS -->
<script src="{{ asset('js/plugins/waypoints.min.js') }}"></script>

<!-- Counter up JS -->
<script src="{{ asset('js/plugins/counterup.min.js') }}"></script>

<!-- Mailchimp JS -->
<script src="{{ asset('js/plugins/mailchimp-ajax-submit.min.js') }}"></script>

<!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->

<!--
<script src="assets/js/plugins/plugins.min.js"></script>
-->

<!-- Main JS -->
<script src="{{ asset('js/main.js') }}"></script>

<!-- Cookie Script -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?thirdparty=1&always=1"></script>
