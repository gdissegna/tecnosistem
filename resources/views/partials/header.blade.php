<!--====================  header area ====================-->
<div class="header-area header-sticky header-sticky--default">
    <div class="header-area__desktop header-area__desktop--default">
        <!--=======  header top bar  =======-->
        <div class="header-top-bar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4">
                        <!-- top bar left -->
                        <div class="top-bar-left-wrapper">
                            <div class="social-links social-links--white-topbar d-inline-block">
                                <ul>
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" class="button">{{ $properties['native'] }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <!-- top bar right -->
                        <div class="top-bar-right-wrapper d-inline-block">
                            <ul>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=======  End of header top bar  =======-->
        <!--=======  header info area  =======-->
        <div class="header-info-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12">
                        <div class="header-info-wrapper align-items-center">
                            <!-- logo -->
                            <div class="logo">
                                <a href="index.html">
                                    <img src="{{ asset('images/logo-tecnomachine.png') }}" class="img-fluid" alt="">
                                </a>
                            </div>

                            <!-- header contact info -->
                            <div class="header-contact-info">
                                <div class="header-info-single-item">
                                    <div class="header-info-single-item__icon">
                                        <i class="zmdi zmdi-smartphone-android"></i>
                                    </div>
                                    <div class="header-info-single-item__content">
                                        <h6 class="header-info-single-item__title">{{ trans('custom.titolo_telefono') }}</h6>
                                        <p class="header-info-single-item__subtitle">+39.0432.755056</p>
                                    </div>
                                </div>
                                <div class="header-info-single-item">
                                    <div class="header-info-single-item__icon">
                                        <i class="zmdi zmdi-home"></i>
                                    </div>
                                    <div class="header-info-single-item__content">
                                        <h6 class="header-info-single-item__title">{{ trans('custom.titolo_indirizzo') }}</h6>
                                        <p class="header-info-single-item__subtitle">Via Thonet, 2 - 33044 Manzano (UD) - Italia</p>
                                    </div>
                                </div>
                            </div>

                            <!-- mobile menu -->
                            <div class="mobile-navigation-icon" id="mobile-menu-trigger">
                                <i></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=======  End of header info area =======-->
        <!--=======  header navigation area  =======-->
        <div class="header-navigation-area default-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- header navigation -->
                        <div class="header-navigation header-navigation--header-default position-relative">
                            <div class="header-navigation__nav position-static">
                                <nav>
                                    <ul>
                                        <li class=""><a href="{{ route('index') }}">{{ trans('custom.home') }}</a></li>
                                        <li><a href="{{ route('azienda') }}"> {{ trans('custom.azienda') }}</a></li>
                                        <li><a href="{{ route('assistenza') }}">{{ trans('custom.assistenza') }}</a></li>
                                        <li><a href="{{ route('newslist') }}"> News</a></li>
                                        <li> <a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=======  End of header navigation area =======-->
    </div>
</div>
<!--====================  End of header area  ====================-->