@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>{{ trans('about.titolo_azienda') }}</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li>{{ trans('about.titolo') }}</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--top--120">
        <!--About section start-->
        <div class="about-section section-space--inner--bottom--120">
            <div class="container">
                <div class="row row-25 align-items-center">

                    <div class="col-lg-6 col-12 mb-30">
                        <div class="about-image-two">
                            <img src="{{ asset('images/azienda/tecnomachine-azienda-thumb.jpg') }}" alt="">
                        </div>
                    </div>

                    <div class="col-lg-6 col-12 mb-30">
                        <div class="about-content-two">
                            <h3>{{ trans('about.titolo') }}</h3>
                            <h1>{{ trans('about.sottotitolo_azienda') }}</h1>
                            {!! trans('about.testo_azienda') !!}
                            <p></p>
                            <a href="service.html" class="ht-btn--default ht-btn--default--dark-hover section-space--top--20">{{ trans('about.button_cta') }}</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--About section end-->
    </div>

@endsection