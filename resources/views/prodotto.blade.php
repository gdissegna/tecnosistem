@extends('master')


@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>{{ $product->name }}</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li><a href="{{ route('categoria',[$product->category->slug]) }}">{{ $product->category->name }}</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--120">
        <!--Projects section start-->
        <div class="project-section">
            <div class="container">
                <div class="row">

                    <div class="col-12 section-space--bottom--40">
                        <div class="project-image"><img src="{{ asset($product->primary_img) }}" class="img-fluid" alt=""></div>
                    </div>

                    <div class="col-lg-12 col-12 section-space--bottom--30 pl-30 pl-sm-15 pl-xs-15">
                        <div class="project-details">
                            <h2>{{ $product->name }}</h2>
                            <h3>{{ trans('product.titolo_codice') }} {{ $product->codice }}</h3>
                            <p>&nbsp;</p>
                            <h3>{{ trans('product.titolo_descrizione') }}</h3>
                            {!! $product->description !!}
                            <p>&nbsp;</p>
                            <a href="{{ route('prodottoDownload',['id' => $product->id]) }}" class="ht-btn ht-btn--round">
                                {{ trans('product.titolo_pdf') }}</a>
                            <a href="{{ route('carrelloAddItem',['id' => $product->id]) }}" class="ht-btn ht-btn--round">
                                {{ trans('product.carrello_button') }}</a>
                        </div>
                    </div>

                    <div class="col-12 section-space--bottoonm--40">
                        <h3>{{ trans('product.titolo_img_aggiuntive') }}</h3>
                        <div class="row row-5 image-popup">
                            @foreach($product->images as $image)
                            <div class="col-xl-3 col-lg-4 col-sm-6 col-12 section-space--top--10">
                                <a href="{{ asset($image->image) }}" class="gallery-item single-gallery-thumb"><img src="{{ asset($image->image) }}" class="img-fluid" alt="{{ $image->alttext }}"><span class="plus"></span></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-12 col-12 section-space--bottom--30 pl-30 pl-sm-15 pl-xs-15">
                        <div class="project-details">
                            <h3>{{ trans('product.titolo_video') }}</h3>
                            {!! $product->video !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--Projects section end-->
    </div>


@endsection