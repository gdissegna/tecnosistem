@extends('master')

@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>{{ trans('about.titolo_azienda') }}</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li>{{ trans('about.titolo') }}</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--top--120">
        <!--About section start-->
        <!--Service section start-->
        <div class="service-section">
            <div class="container">

                <div class="row">

                    <div class="col-lg-12 col-12 order-1 order-lg-2">

                        <div class="service-details">

                            <div class="service-gallery">
                                <div class="swiper-container service-gallery__container">
                                    <div class="swiper-wrapper service-gallery__wrapper">
                                        <div class="swiper-slide service-gallery__single-slide">
                                            <div class="item">
                                                <img src="{{ asset('images/assistenza/tecnomachine-assistenza-slider-1.jpg') }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide service-gallery__single-slide">
                                            <div class="item">
                                                <img src="{{ asset('images/assistenza/tecnomachine-assistenza-slider-2.jpg') }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide service-gallery__single-slide">
                                            <div class="item">
                                                <img src="{{ asset('images/assistenza/tecnomachine-assistenza-slider-3.jpg') }}" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="ht-swiper-button-prev ht-swiper-button-prev-14 ht-swiper-button-nav"><i class="ion-ios-arrow-left"></i></div>
                                <div class="ht-swiper-button-next ht-swiper-button-next-14 ht-swiper-button-nav"><i class="ion-ios-arrow-forward"></i></div>
                            </div>

                            <div class="content section-space--top--30">
                                <div class="row">
                                    <div class="col-12">
                                        <h2>I nostri servizi</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, sunt perspiciatis error id ipsa atque unde quis dolore nobis eum aperiam enim blanditiis pariatur inventore eius commodi consectetur ut. Totam, assumenda! Laboriosam possimus, corporis dicta!</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores aliquid quod, officiis unde nostrum itaque! Adipisci dolorum, ab dolor, exercitationem praesentium dolorem quo voluptatum itaque dignissimos, sit esse cupiditate. Doloremque rerum similique a nobis placeat in illum, quo quaerat, ut repellat, fuga itaque? Nihil mollitia nisi, nam, accusantium nemo consequuntur reiciendis autem dicta consequatur earum beatae dolor distinctio, debitis repudiandae?</p>
                                    </div>
                                    <div class="col-lg-6 col-12 section-space--top--30">
                                        <h3>Controllo meccanico</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, animi? Vel quas in minima qui totam, aliquid dolores quaerat voluptatum?</p>
                                    </div>
                                    <div class="col-lg-6 col-12 section-space--top--30">
                                        <h3>Controllo elettronico</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, animi? Vel quas in minima qui totam, aliquid dolores quaerat voluptatum?</p>
                                    </div>
                                    <div class="col-lg-6 col-12 section-space--top--30">
                                        <h3>Sostituzione con ricambi</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, animi? Vel quas in minima qui totam, aliquid dolores quaerat voluptatum?</p>
                                    </div>
                                    <div class="col-lg-6 col-12 section-space--top--30">
                                        <h3>Sabbiatura e riverniciatura</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, animi? Vel quas in minima qui totam, aliquid dolores quaerat voluptatum?</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    

                </div>

            </div>
        </div>
        <!--Service section end-->
        <!--About section end-->
    </div>

@endsection