@extends('master')


@section('content')

    <!--====================  breadcrumb area ====================-->
    <div class="breadcrumb-area bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="page-banner text-center">
                        <h1>{{ trans('contatti.contatti_title') }}</h1>
                        <ul class="page-breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li>{{ trans('contatti.contatti_contatti') }}</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <div class="page-wrapper section-space--inner--120">
        <!--Contact section start-->
        <div class="conact-section">
            <div class="container">

                <div class="row section-space--bottom--50">
                    <div class="col">
                        <div id="contact-map" class="contact-map">

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-12">
                        <div class="contact-information">
                            <h3>{{ trans('contatti.contatti_title') }}</h3>
                            <ul>
                                <li>
                                    <span class="icon"><i class="ion-android-map"></i></span>
                                    <span class="text"><span>Via Thonet, 2 - 33044 Manzano (UD) - Italia</span></span>
                                </li>
                                <li>
                                    <span class="icon"><i class="ion-ios-telephone-outline"></i></span>
                                    <span class="text">{{ trans('custom.telefono') }} (+39) 0432.755056</span>
                                </li>
                                <li>
                                    <span class="icon"><i class="ion-ios-email-outline"></i></span>
                                    <span class="text"><a href="mailto:info@tecnomachine.it">info@tecnomachine.it</a><a href="http://www.tecnomachine.it" target="_blank">www.tecnomachine.it</a></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="contact-form">
                            <h3>{{ trans('contatti.contatti_form_title') }}</h3>
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert-box alert radius">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form id="contact-form" action="{{ route('contattipost') }}" method="post">
                                <div class="row row-10">
                                    <input class="hidden" name="_token" type="hidden" value="{{ csrf_token() }}">
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="name" type="text" placeholder="{{ trans('contatti.label_nome') }}"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="email" type="email" placeholder="{{ trans('contatti.label_email') }}"></div>
                                    <div class="col-md-12 col-12 section-space--bottom--20"><input name="azienda" type="text" placeholder="{{ trans('contatti.label_societa') }}"></div>
                                    <div class="col-md-12 col-12 section-space--bottom--20"><input name="indirizzo" type="text" placeholder="{{ trans('contatti.label_indirizzo') }}"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="citta" type="text" placeholder="{{ trans('contatti.label_citta') }}"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="paese" type="text" placeholder="{{ trans('contatti.label_paese') }}"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="phone" type="text" placeholder="{{ trans('contatti.label_telefono') }}"></div>
                                    <div class="col-md-6 col-12 section-space--bottom--20"><input name="fax" type="text" placeholder="{{ trans('contatti.label_fax') }}"></div>
                                    <div class="col-md-12 col-12 section-space--bottom--20">
                                        <select name="attivita" type="text">
                                            <option value="" disabled selected>{{ trans('contatti.label_attivita') }}</option>
                                            <option>{{ trans('contatti.label_rivenditore') }}</option>
                                            <option>{{ trans('contatti.label_utilizzatore') }}</option>
                                            <option>{{ trans('contatti.label_costruttore') }}</option>
                                        </select>
                                    </div>
                                    <div class="col-12 section-space--bottom--20"><textarea name="message" placeholder="{{ trans('contatti.label_messaggio') }}"></textarea></div>
                                    <div class="col-md-12 col-12 section-space--bottom--20">
                                        {!! Recaptcha::render() !!}
                                    </div>
                                    <div class="col-12"><button>{{ trans('contatti.label_invia') }}</button></div>
                                </div>
                            </form>
                            <p class="form-message"></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Contact section end-->
    </div>

@endsection