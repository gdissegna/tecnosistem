@extends('master')

@section('content')
        <!-- Slider Revolution -->
<div class="revolution-slider-container">
    <div class="revolution-slider">
        <ul style="display: none;">
            <!-- SLIDE 1 -->
            <li data-transition="fade" data-masterspeed="500" data-slotamount="1" data-delay="6000">
                <!-- MAIN IMAGE -->
                <img src="{{ asset('images/slider-01.jpg') }}" alt="slidebg1" data-bgfit="cover">
                <!-- LAYERS -->
                <!-- / -->
            </li>
        </ul>
    </div>
</div>
<!--/-->


<div class="row margin-top-70">
    <div class="column column-1-2">
        <h4 class="box-header margin-top-26">{{ trans('about.titolo') }}</h4>

        <p class="description margin-top-0">{{ trans('about.testo') }}</p>

    </div>
    <div class="column column-1-2">
        <img src="{{ asset("images/azienda/slider-1.jpg") }}" class="img-responsive" height="380" width="570" alt=""
             title=""/>
    </div>
</div>
<!-- end about us -->
<div class="clearfix">
    <div class="row padding-top-70">
        <h2 class="box-header">{{ trans('custom.prodotti') }}</h2>
        <ul class="services-list clearfix padding-top-30">
            
            @foreach($products as $product)
                <li>
                    <a href="{{ asset($product->primary_img) }}" title="">
                        <img class="img-responsive" src="{{ asset($product->primary_img) }}" alt="">
                    </a>
                    <h4 class="box-header"><a
                                href="{{  route('prodotto',[ $product->category->slug , $product->slug]) }}"
                                title="{{ $product->name }}">{{ $product->name }}<span class="template-arrow-menu"></span></a>
                    </h4>
                </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection

