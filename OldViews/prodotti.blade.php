@extends('master')

@section('content')
    <div class="theme-page padding-bottom-70">
        <div class="row gray full-width page-header vertical-align-table">
            <div class="row full-width padding-top-bottom-50 vertical-align-cell">
                <div class="row">
                    <div class="page-header-left">
                        <h1>{{ trans('custom.prodotto') }}</h1>
                    </div>
                    <div class="page-header-right">
                        <div class="bread-crumb-container">
                            <label></label>
                            <ul class="bread-crumb">
                                <li>
                                    <a title="{{ route('index') }}" href="{{ route('index') }}">
                                        {{ trans('custom.index') }}
                                    </a>
                                </li>
                                <li class="separator">
                                    &#47;
                                </li>
                                <li>
                                    {{ trans('custom.prodotti') }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
                <div class="row">
                    <ul class="services-list clearfix padding-top-70">
                        @foreach($category->products as $product)
                        <li>
                            <a href="{{ asset($product->primary_img) }}" title="{{ $product->name }}">
                                <img src="{{ asset($product->primary_img) }}" alt="">
                            </a>
                            <h4 class="box-header"><a href="{{ route('prodotto',[$product->category->slug, $product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}<span class="template-arrow-menu"></span></a></h4>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection