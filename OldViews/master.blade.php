<!DOCTYPE html>
<html>
<head lang="{{ App::getLocale() }}">
    <title>{{ trans('custom.site-title') }}</title>
    <!--meta-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="keywords" content="{{ trans('custom.keywords') }}" />
    <meta name="description" content="{{ trans('custom.description') }}" />
    @include('partials.css')
</head>

<body>
<div class="site-container">
    <div class="header-top-bar-container clearfix">
        <div class="header-top-bar">
            <ul class="contact-details clearfix">
                <li class="template-phone">
                    (+39) 0432755056
                </li>
                <li class="template-mail">
                    <a href="mailto:info@tecnosistem.it">info@tecnosistem.it</a>
                </li>
                <li class="template-clock">
                    &nbsp;
                </li>
            </ul>
            <ul class="social-icons">
                // macmara language selector
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    <li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" class="button">{{ $properties['native'] }}</a></li>
                @endforeach
            </ul>
        </div>
        <a href="#" class="header-toggle template-arrow-up"></a>
    </div>
    <div class="header-container">
        <!--<div class="header-container sticky">-->
        <div class="vertical-align-table column-1-1">
            <div class="header clearfix">
                <div class="logo vertical-align-cell">
                    <h1><a href="logo" title="CarService"><img src="{{ asset('images/Logo.png') }}"> </a></h1>
                </div>
                <a href="#" class="mobile-menu-switch vertical-align-cell">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </a>
                <div class="menu-container clearfix vertical-align-cell">
                    <nav>
                        <ul class="sf-menu">
                            <li class="selected">
                                <a href="{{ route('index') }}" title="Home">
                                    {{ trans('custom.home') }}
                                </a>
                            </li>
                            <li class="selected">
                                <a href="{{ route('azienda') }}" title="Services">
                                    {{ trans('custom.azienda') }}
                                </a>
                            </li>
                            <li>
                                <a href="#" title="Services">
                                    {{ trans('custom.prodotti') }}
                                </a>
                                <ul>
                                    @foreach($menu_categories as $menu_category)
                                    <li>
                                        <a href="{{ route('categoria',[$menu_category->slug]) }}" title="{{ $menu_category->name }}">
                                            {{ $menu_category->name }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a>
                            </li>
                            
                        </ul>
                    </nav>
                    <div class="mobile-menu-container">
                        <div class="mobile-menu-divider"></div>
                        <nav>
                            <ul class="mobile-menu">
                                <li class="selected">
                                    <a href="{{ route('index') }}" title="Home">
                                        {{ trans('custom.home') }}
                                    </a>
                                </li>
                                <li class="selected">
                                    <a href="{{ route('azienda') }}" title="Services">
                                        {{ trans('custom.azienda') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Services">
                                        {{ trans('custom.prodotti') }}
                                    </a>
                                    <ul>
                                        @foreach($menu_categories as $menu_category)
                                            <li>
                                                <a href="{{ route('categoria',[$menu_category->slug]) }}" title="{{ $menu_category->name }}">
                                                    {{ $menu_category->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ route('contatti') }}">{{ trans('custom.contatti') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('carrello') }}">{{ trans('custom.carrello') }}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT START -->
    @yield('content')
    <!-- CONTENT END -->
    <div class="row dark-gray footer-row full-width padding-top-30 padding-bottom-50">
        <div class="row row-4-4 top-border page-padding-top">
            <div class="column column-1-3">
                <h6 class="box-header">{{ trans('custom.contatti') }}</h6>
                <ul class="list simple margin-top-20">
                    <li>Via Thonet, 4</li>
                    <li>33044 Manzano (UD) Italy</li>
                    <li><span>{{ trans('telefono') }}:</span>(+39) 0432.755056</li>
                    <li><span>{{ trans('custom.assistenza_home') }}:</span>(+39) 0432.755056</li>
                    <li><span>E-mail:</span><a href="mailto:info@tecnosistem.it">info@tecnosistem.it</a></li>
                    <li>P. IVA IT02746890306</li>
                    <li>C.F. ZMRMCL60R13L483H</li>
                </ul>
            </div>
            <div class="column column-1-3">
                <h6 class="box-header">{{ trans('custom.nostri_servizi') }}</h6>
                <ul class="list margin-top-20">
                    <li class="template-bullet">Riparazione macchinari</li>
                    <li class="template-bullet">Revisione periodica</li>
                    <li class="template-bullet">Consulenza progettuale</li>
                    <li class="template-bullet">Vendita su commissione</li>
                </ul>
            </div>
            <div class="column column-1-3">
                <h6 class="box-header">{{ trans('custom.orari') }}</h6>
                <ul class="list simple margin-top-20">
                    <li><span>Lunedì:</span>7:30am - 5:30pm</li>
                    <li><span>Martedì:</span>7:30am - 5:30pm</li>
                    <li><span>Mercoledì:</span>7:30am - 5:30pm</li>
                    <li><span>Giovedì:</span>7:30am - 5:30pm</li>
                    <li><span>Venerdì:</span>7:30am - 5:30pm</li>
                    <li><span>Sabato:</span>Chiuso</li>
                    <li><span>Domenica:</span>Chiuso</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row align-center padding-top-bottom-30">
        <span class="copyright">© Copyright 2015 <a href="" title="Tecnosistem" target="_blank">Tecnosistem</a> by <a href="http://www.triangolo.it" title="Omega" target="_blank">Omega</a></span>
    </div>
</div>
<a href="#top" class="scroll-top animated-element template-arrow-up" title="Scroll to top"></a>
<div class="background-overlay"></div>
<!-- JS -->
    @include('partials.js')
</body>
</html>