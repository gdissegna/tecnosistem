@extends('master')

@section('content')
    <div class="theme-page">
        <div class="row gray full-width page-header vertical-align-table">
            <div class="row full-width padding-top-bottom-50 vertical-align-cell">
                <div class="row">
                    <div class="page-header-left">
                        <h1>{{ $product->name }}</h1>
                    </div>
                    <div class="page-header-right">
                        <div class="bread-crumb-container">
                            <label>YOU ARE HERE:</label>
                            <ul class="bread-crumb">
                                <li>
                                    <a title="HOME" href="{{ route('index') }}">
                                        HOME
                                    </a>
                                </li>
                                <li class="separator">
                                    &#47;
                                </li>
                                <li>
                                    <a title="Our Services" href="{{ route('categoria', [$product->category->slug]) }}">
                                        {{ $product->category->name }}
                                    </a>
                                </li>
                                <li class="separator">
                                    &#47;
                                </li>
                                <li>
                                    {{ $product->name }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="row margin-top-70">
                <div class="column column-1-4">
                    <ul class="vertical-menu">
                       @foreach($menu_products as $menu_product)
                        @if($menu_product->id == $product->id)
                        <li class="selected">
                            <a href="#" title="Engine Diagnostics">
                                {{ $menu_product->name }}
                                <span class="template-arrow-menu"></span>
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="{{ route('prodotto',[$menu_product->category->slug, $menu_product->slug]) }}" title="Lube, Oil and Filters">
                                {{ $menu_product->name }}
                                <span class="template-arrow-menu"></span>
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                    <h6 class="box-header page-margin-top">DOWNLOAD</h6>
                    <ul class="buttons margin-top-30">
                        <li class="template-arrow-circle-down">
                            <a href="#" title="Download Brochure">Download Brochure</a>
                        </li>
                    </ul>
                </div>
                <div class="column column-3-4">
                    <div class="row">
                        <div class="column column-1-1">
                            <a href="{{ asset($product->primary_img) }}" class="prettyPhoto re-preload" title="{{ $product->name }}">
                                <img src='{{ asset($product->primary_img) }}' alt='img'>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($product->images as $image )
                        <div class="column column-1-2">
                            <a href="{{ $image->image }}" class="prettyPhoto re-preload" title="Engine Diagnostics">
                                <img src='{{ $image->image }}' alt='img'>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <div class="row page-margin-top">
                        <div class="column-1-1">
                            <h3 class="box-header">{{ $product->name }}</h3>
                            <p class="margin-top-20">{{ $product->description }}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

