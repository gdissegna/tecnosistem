@extends('master)

@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">{{ trans('') }}</h1>
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                        <li class="active">{{ trans('') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-9 post-list">
                    <!-- post -->
                    @foreach($articles as $article)
                    <div class="post-item">
                        <div class="post-image pull-left">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="{{ $article->primary_img }}" width="320" height="282" alt="" title="" />
                                    </div>
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="fa fa-angle-left fa-2x" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span></a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="fa fa-angle-right fa-2x" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span></a></div>
                        </div>
                        <h2 class="post-title">
                            <a href="blog-single.html">{{ $article->title }}</a>
                        </h2>
                        <div class="post-content">{{ $article->body }}</div>
                    </div>
                    @endforeach
                    <!-- post -->
                    <!-- post -->
                 </div>
                </div>
                <div class="sidebar col-sm-12 col-md-3">
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">Category</h3>
                        </div>
                        <div id="MainMenu2">
                            <div class="list-group panel">
                                <div class="collapse in" id="demo3">
                                    @foreach($articles as $article)
                                    <a href="{{ route('singlenews',[$article->slug]) }}" class="list-group-item">{{ $article->title }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- category-list -->
                    </div>
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">Recent Posts</h3>
                        </div>
                        <ul class="latest-posts">
                            <li>
                                <div class="post-thumb">
                                    <img class="img-rounded" src="img/sections/blog/1.jpg" alt="" title="" width="84"
                                         height="84" />
                                </div>
                                <div class="post-details">
                                    <div class="description">
                                        <a href="#">
                                            <!-- Text -->
                                            The standard chunk of Lorem Ipsum used since .</a>
                                    </div>
                                    <div class="meta">
                                        <!-- Meta Date -->

                                        <span class="time">
                                        <i class="fa fa-calendar"></i> 03.11.2014</span></div>
                                </div>
                            </li>
                            <li>
                                <div class="post-thumb">
                                    <img class="img-rounded" src="img/sections/blog/2.jpg" alt="" title="" width="84"
                                         height="84" />
                                </div>
                                <div class="post-details">
                                    <div class="description">
                                        <a href="#">
                                            <!-- Text -->
                                            The standard chunk of Lorem Ipsum used since.</a>
                                    </div>
                                    <div class="meta">
                                        <!-- Meta Date -->

                                        <span class="time">
                                        <i class="fa fa-calendar"></i> 03.11.2014</span></div>
                                </div>
                            </li>
                            <li>
                                <div class="post-thumb">
                                    <img class="img-rounded" src="img/sections/blog/3.jpg" alt="" title="" width="84"
                                         height="84" />
                                </div>
                                <div class="post-details">
                                    <div class="description">
                                        <a href="#">
                                            <!-- Text -->
                                            The standard chunk of Lorem Ipsum used since.</a>
                                    </div>
                                    <div class="meta">
                                        <!-- Meta Date -->

                                        <span class="time">
                                        <i class="fa fa-calendar"></i> 03.11.2014</span></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">Gallery</h3>
                        </div>
                        <div class="owl-carousel navigation-4" data-pagination="false" data-items="1" data-autoplay="true"
                             data-navigation="true">
                            <img src="img/sections/blog/1.jpg" width="270" height="270" alt="" />
                            <img src="img/sections/blog/2.jpg" width="270" height="270" alt="" /></div>
                    </div>
                    <div class="widget">
                        <div class="widget-title">
                            <h3 class="title">Tags</h3>
                        </div>
                        <ul class="tags">
                            <li>
                                <a href="#">Corporate</a>
                            </li>
                            <li>
                                <a href="#">business</a>
                            </li>
                            <li>
                                <a href="#">agency</a>
                            </li>
                            <li>
                                <a href="#">medical</a>
                            </li>
                            <li>
                                <a href="#">studio</a>
                            </li>
                            <li>
                                <a href="#">university</a>
                            </li>
                            <li>
                                <a href="#">Governments</a>
                            </li>
                            <li>
                                <a href="#">charity</a>
                            </li>
                            <li>
                                <a href="#">realestate</a>
                            </li>
                            <li>
                                <a href="#">app</a>
                            </li>
                            <li>
                                <a href="#">restaurant</a>
                            </li>
                            <li>
                                <a href="#">fitness</a>
                            </li>
                            <li>
                                <a href="#">general</a>
                            </li>
                            <li>
                                <a href="#">construction</a>
                            </li>
                            <li>
                                <a href="#">School</a>
                            </li>
                            <li>
                                <a href="#">healthcare</a>
                            </li>
                        </ul>
                    </div>
                    <div class="widget my-feeds">
                        <div class="widget-title">
                            <h3 class="title">Instagram Photos</h3>
                        </div>
                        <div class="social-feed instagram-feed"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page-section -->
@endsection