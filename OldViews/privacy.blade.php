@extends('master')

@section('content')
    <div class="page-header page-title-left page-title-pattern">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">About Us</h1>
                    <h5>A Short Page title</h5>
                    <ul class="breadcrumb">
                        <li>
                            <a href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="active">{{ trans('custom.privacy') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- page-header -->
    <section id="about-us" class="page-section">
        <div class="container">
            <h3 class="text-center">{{ trans('privacy.titolo') }}</h3>
            <p class="text-center">{{ trans('privacy.subtitle') }}</p>
            <br />
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <!-- Wrapper for slides -->
                <!-- Controls -->
                <div class="row">
                    <div class="col-md-12 content-block">
                        <h1></h1>
                        <p class="text-justify">{!!  trans('privacy.testo') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- page-section -->
@endsection